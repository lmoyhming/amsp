//
//  AMTabBarPosition.h
//  AMTabBarController
//
//  Created by Tretter Matthias on 24.04.12.
//  Copyright (c) 2012 NOUS Wissensmanagement GmbH. All rights reserved.
//

typedef enum {
    AMTabBarPositionTop = 0,
    AMTabBarPositionRight,
    AMTabBarPositionBottom,
    AMTabBarPositionLeft,
} AMTabBarPosition;

#define kAMTabBarPositionDefault    AMTabBarPositionLeft


NS_INLINE BOOL AMTabBarIsVertical(AMTabBarPosition position) {
    return position == AMTabBarPositionLeft || position == AMTabBarPositionRight;
}

NS_INLINE BOOL AMTabBarIsHorizontal(AMTabBarPosition position) {
    return position == AMTabBarPositionTop || position == AMTabBarPositionBottom;
}
