//
//  AMTabBarItem.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "AMTabBarItem.h"


#define kAMDefaultTintColor                 [UIColor colorWithRed:41.0/255.0 green:147.0/255.0 blue:239.0/255.0 alpha:0.0]
#define kAMDefaultTitleColor                [UIColor whiteColor]
#define kAMDefaultSelectedTitleColor        [UIColor whiteColor]
#define kAMImageOffset                       -5.f

@interface AMTabBarItem () {
    BOOL _selectedByUser;
}

@property (nonatomic, strong) UILabel *titleLabel;

@end


@implementation AMTabBarItem

@synthesize selected = _selected;
@synthesize selectedImageTintColor = _selectedImageTintColor;
@synthesize titleColor = _titleColor;
@synthesize selectedTitleColor = _selectedTitleColor;
@synthesize image = _image;
@synthesize titleLabel = _titleLabel;

////////////////////////////////////////////////////////////////////////
#pragma mark - Lifecycle
////////////////////////////////////////////////////////////////////////

+ (AMTabBarItem *)itemWithTitle:(NSString *)title image:(UIImage *)image {
    AMTabBarItem *item = [[AMTabBarItem alloc] initWithFrame:CGRectZero];
    
    item.title = title;
    item.image = image;
    
    return item;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        
        _selectedByUser = NO;
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = AMFONT(11);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor whiteColor];
        [self addSubview:_titleLabel];
    }
    
    return self;
}

////////////////////////////////////////////////////////////////////////
#pragma mark - UIView
////////////////////////////////////////////////////////////////////////

- (void)layoutSubviews {
    [super layoutSubviews];
    if ([self.title isEqualToString:@"Home"]) {
//        self.center = CGPointMake(self.center.x, self.center.y + 10);
    }
    if (self.image != nil) {
        CGFloat imageOffset = self.title.length > 0 ? kAMImageOffset : 0.f;
        CGFloat textTop = floor((self.bounds.size.height - self.image.size.height)/2.f) - imageOffset + self.image.size.height + 2.f;
        CGFloat offsetTop = [self.title isEqualToString:@"Home"] ? 10 : 10;
        self.titleLabel.frame = CGRectMake(0.f, textTop, self.bounds.size.width, self.titleLabel.font.lineHeight);
    } else {
        self.titleLabel.frame = self.bounds;
    }
}

- (void)drawRect:(CGRect)rect {
    
    CGRect bounds = self.bounds;
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // draw image overlay?
    if (self.image != nil) {
        CGContextSaveGState(context);
        
        // flip the coordinates system
        CGContextTranslateCTM(context, 0.f, bounds.size.height);
        CGContextScaleCTM(context, 1.f, -1.f);
        
        // draw an image in the center of the cell (offset to the top)
        CGSize imageSize = self.image.size;
        CGFloat imageOffset = [self.title isEqualToString:@"Home"] ? kAMImageOffset : kAMImageOffset;
        CGRect imageRect = CGRectMake(floorf(((bounds.size.width-imageSize.width)/2.f)),
                                      floorf(((bounds.size.height-imageSize.height)/2.f)) + imageOffset,
                                      imageSize.width,
                                      imageSize.height);
        CGContextDrawImage(context, imageRect, self.image.CGImage);
        CGContextRestoreGState(context);
    }
}

////////////////////////////////////////////////////////////////////////
#pragma mark - UIControl
////////////////////////////////////////////////////////////////////////

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    // somehow self.selected always returns NO, so we store it in our own iVar
    _selectedByUser = selected;
    
    if (selected) {
        self.titleLabel.textColor = self.selectedTitleColor;
    } else {
        self.titleLabel.textColor = self.titleColor;
    }
    
    [self setNeedsDisplay];
}

////////////////////////////////////////////////////////////////////////
#pragma mark - AMTabBarItem
////////////////////////////////////////////////////////////////////////

- (void)setTitle:(NSString *)title {
    self.titleLabel.text = title;
    [self setNeedsLayout];
}

- (NSString *)title {
    return self.titleLabel.text;
}

- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
    
    [self setNeedsDisplay];
    [self setNeedsLayout];
}

- (UIColor *)selectedImageTintColor {
    return _selectedImageTintColor ?: kAMDefaultTintColor;
}

- (UIColor *)titleColor {
    return _titleColor ?: kAMDefaultTitleColor;
}

- (UIColor *)selectedTitleColor {
    return _selectedTitleColor ?: kAMDefaultSelectedTitleColor;
}

@end
