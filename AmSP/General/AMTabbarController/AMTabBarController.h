//
//  AMTabBarController.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AMTabBar.h"
#import "AMTabBarItem.h"
#import "AMTabBarPosition.h"
#import "UIViewController+AMTabBarItem.h"
#import "AMTabBarControllerDelegate.h"
#import "AMTabBarControllerAnimation.h"

@interface AMTabBarController : UIViewController

@property (nonatomic, copy) NSArray *viewControllers;

@property (nonatomic, assign) NSUInteger selectedIndex;

@property (nonatomic, unsafe_unretained) UIViewController *selectedViewController;

@property (nonatomic, assign) id<AMTabBarControllerDelegate> delegate;

@property (nonatomic, strong, readonly) AMTabBar *tabBar;
@property (nonatomic, assign) AMTabBarPosition tabBarPosition;

@property (nonatomic, assign) AMTabBarControllerAnimation animation;

@property (nonatomic, assign) NSTimeInterval animationDuration;

@property (nonatomic, assign) BOOL tabBarHidden;

- (instancetype)initWithDelegate:(id<AMTabBarControllerDelegate>)delegate;
- (void)setTabBarHidden:(BOOL)tabBarHidden animated:(BOOL)animated;

@end
