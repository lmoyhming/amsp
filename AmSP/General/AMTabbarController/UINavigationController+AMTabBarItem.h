//
//  UINavigationController+AMTabBarItem.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AMTabBarItem;

@interface UINavigationController (AMTabBarItem)

@property (nonatomic, strong, setter = ng_setTabBarItem:) AMTabBarItem *ng_tabBarItem;

@end
