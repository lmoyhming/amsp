//
//  AMTabBar.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMTabBarPosition.h"

typedef enum {
    AMTabBarLayoutStrategyStrungTogether        = 0,
    AMTabBarLayoutStrategyEvenlyDistributed,
    AMTabBarLayoutStrategyCentered
} AMTabBarLayoutStrategy;


@interface AMTabBar : UIScrollView

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, assign) NSUInteger selectedItemIndex;
@property (nonatomic, assign) AMTabBarPosition position;
@property (nonatomic, assign) AMTabBarLayoutStrategy layoutStrategy;
/** the padding to apply between items, not taken into account when layoutStrategy is EvenlyDistributed */
@property (nonatomic, assign) CGFloat itemPadding;

/** defaults to black */
@property (nonatomic, strong) UIColor *tintColor;
/** defaults to nil */
@property (nonatomic, strong) UIImage *backgroundImage;
/** flag whether the semi-transparent item highlight is shown */
@property (nonatomic, assign) BOOL showsItemHighlight;
/** defaults to white */
@property (nonatomic, strong) UIColor *itemHighlightColor;

- (void)selectItemAtIndex:(NSUInteger)index;
- (void)deselectSelectedItem;

@end
