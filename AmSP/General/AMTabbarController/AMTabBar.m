//
//  AMTabBar.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "AMTabBar.h"
#import "AMTabBarItem.h"
#import <QuartzCore/QuartzCore.h>


#define kAMDefaultTintColor             [UIColor blackColor]
#define kAMDefaultItemHighlightColor    [UIColor colorWithWhite:1.f alpha:0.2f]


@interface AMTabBar () {
    CGGradientRef _gradientRef;
    UIImageView *bgImageView;
}

@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIView *itemHighlightView;

- (void)createGradient;
- (void)updateItemHighlight;
- (CGFloat)dimensionToBeConsideredOfItem:(AMTabBarItem *)item;

@end


@implementation AMTabBar

@synthesize items = _items;
@synthesize selectedItemIndex = _selectedItemIndex;
@synthesize position = _position;
@synthesize layoutStrategy = _layoutStrategy;
@synthesize itemPadding = _itemPadding;
@synthesize tintColor = _tintColor;
@synthesize backgroundImage = _backgroundImage;
@synthesize backgroundView = _backgroundView;
@synthesize itemHighlightView = _itemHighlightView;
@synthesize showsItemHighlight = _showsItemHighlight;
@synthesize itemHighlightColor = _itemHighlightColor;

////////////////////////////////////////////////////////////////////////
#pragma mark - Lifecycle
////////////////////////////////////////////////////////////////////////

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.alwaysBounceHorizontal = NO;
        self.clipsToBounds = YES;
        
        _selectedItemIndex = 0;
        _layoutStrategy = AMTabBarLayoutStrategyStrungTogether;
        _itemPadding = 0.f;
        _position = kAMTabBarPositionDefault;
        _showsItemHighlight = YES;
        
//        [self createGradient];
        [self updateItemHighlight];
        
    }
    
    return self;
}

- (void)dealloc {
    if (_gradientRef != NULL) {
        CFRelease(_gradientRef);
    }
}

////////////////////////////////////////////////////////////////////////
#pragma mark - UIView
////////////////////////////////////////////////////////////////////////

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat currentFrameLeft = 0.f;
    CGFloat currentFrameTop = 0.f;
    CGFloat totalDimension = 0.f;
    // we change item padding in strategy evenly distributed but don't want to change iVar
    CGFloat appliedItemPadding = self.itemPadding;
    
    // backgroundView gets same frame as tabBar
    self.backgroundView.frame = self.bounds;
    
    if (self.layoutStrategy == AMTabBarLayoutStrategyEvenlyDistributed || self.layoutStrategy == AMTabBarLayoutStrategyCentered) {
        // compute total dimension needed
        for (AMTabBarItem *item in self.items) {
            totalDimension += [self dimensionToBeConsideredOfItem:item];
            
            // we don't take padding only into account if we want to evenly distribute items
            if (self.layoutStrategy != AMTabBarLayoutStrategyEvenlyDistributed) {
                totalDimension += self.itemPadding;
            }
        }
        
        // for evenly distributed items we calculate a new item padding
        if (self.layoutStrategy == AMTabBarLayoutStrategyEvenlyDistributed) {
            // the total padding needed for the whole tabBar
            CGFloat totalPadding = AMTabBarIsVertical(self.position) ? self.bounds.size.height - totalDimension : self.bounds.size.width - totalDimension;
            
            // we apply the padding (items.count - 1) times (always between two items)
            if (self.items.count > 1) {
                appliedItemPadding = MAX(0.f,totalPadding / (self.items.count - 1));
            }
        }
        
        else if (self.layoutStrategy == AMTabBarLayoutStrategyCentered) {
            // we only add padding between icons but we added it for each item in the loop above
            totalDimension -= appliedItemPadding;
            
            if (AMTabBarIsVertical(self.position)) {
                currentFrameTop = floorf((self.bounds.size.height-totalDimension)/2.f);
            } else {
                currentFrameLeft = floorf((self.bounds.size.width-totalDimension)/2.f);
            }
        }
    }
    
    // re-position each item starting from current top/left
    for (AMTabBarItem *item in self.items) {
        CGRect frame = item.frame;
        
        frame.origin.y = currentFrameTop;
        frame.origin.x = currentFrameLeft;
        item.frame = frame;
        
        // move to next item position
        if (AMTabBarIsVertical(self.position)) {
            currentFrameTop += frame.size.height;
        } else {
            currentFrameLeft += frame.size.width;  
            currentFrameLeft += appliedItemPadding;
        }
    }
    
    // re-compute content size
    AMTabBarItem *lastItem = [self.items lastObject];
    
    if (AMTabBarIsVertical(self.position)) {
        self.contentSize = CGSizeMake(lastItem.frame.size.width, lastItem.frame.origin.y + lastItem.frame.size.height);
    } else {
        self.contentSize = CGSizeMake(lastItem.frame.origin.x + lastItem.frame.size.width, lastItem.frame.size.height);
    }
    
    [self updateItemHighlight];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (self.backgroundImage == nil) {
        CGRect bounds = self.bounds;
        CGPoint start;
        CGPoint end;
        
        // draw gradient
        CGContextSaveGState(context);
        CGContextClipToRect(context, bounds);
        
        if (self.position == AMTabBarPositionBottom) {
            start = CGPointMake(bounds.origin.x, bounds.origin.y);
            end = CGPointMake(bounds.origin.x, bounds.origin.y + bounds.size.height);
        } else if (self.position == AMTabBarPositionTop) {
            start = CGPointMake(bounds.origin.x, bounds.origin.y + bounds.size.height);
            end = CGPointMake(bounds.origin.x, bounds.origin.y);
        } else if (self.position == AMTabBarPositionLeft) {
            start = CGPointMake(bounds.origin.x + bounds.size.width, bounds.origin.y);
            end = CGPointMake(bounds.origin.x, bounds.origin.y);
        } else if (self.position == AMTabBarPositionRight) {
            start = CGPointMake(bounds.origin.x, bounds.origin.y);
            end = CGPointMake(bounds.origin.x + bounds.size.width, bounds.origin.y);
        }
        
        CGContextDrawLinearGradient(context, _gradientRef, start, end, 0);
        CGContextRestoreGState(context);
    }
}

////////////////////////////////////////////////////////////////////////
#pragma mark - AMTabBar
////////////////////////////////////////////////////////////////////////

- (void)setItems:(NSArray *)items {
    if (items != _items) {
        [_items performSelector:@selector(removeFromSuperview)];
        
        _items = items;
        
        for (AMTabBarItem *item in _items) {
            [self addSubview:item];
        }
        
        [self setNeedsLayout];
    }
}

- (void)setPosition:(AMTabBarPosition)position {
    if (position != _position) {
        _position = position;
        
        if (AMTabBarIsVertical(position)) {
            self.alwaysBounceVertical = YES;
        } else {
            self.alwaysBounceVertical = NO;
        }
        
        [self setNeedsLayout];
        [self setNeedsDisplay];
    }
}

- (void)selectItemAtIndex:(NSUInteger)index {
    [self deselectSelectedItem];
    
    if (index < self.items.count) {
        AMTabBarItem *item = [self.items objectAtIndex:index];
        item.selected = YES;
        
        self.selectedItemIndex = index;
        [self updateItemHighlight];
    }
}

- (void)deselectSelectedItem {
    if (self.selectedItemIndex < self.items.count) {
        AMTabBarItem *selectedItem = [self.items objectAtIndex:self.selectedItemIndex];
        
        selectedItem.selected = NO;
        self.selectedItemIndex = NSNotFound;
        [self updateItemHighlight];
    }
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    if (backgroundImage != _backgroundImage) {
        [self.backgroundView removeFromSuperview];
        _backgroundImage = backgroundImage;
        
        if (backgroundImage != nil) {
            // is the image a non-resizable image?
            if (UIEdgeInsetsEqualToEdgeInsets(backgroundImage.capInsets,UIEdgeInsetsZero)) {
                self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
                self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
                [self insertSubview:self.backgroundView atIndex:0];
            } else {
                self.backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
            }
        } else {
            self.backgroundView = nil;
        }
    }
}

- (UIColor *)tintColor {
    return _tintColor ?: kAMDefaultTintColor;
}

- (void)setTintColor:(UIColor *)tintColor {
    if (tintColor != _tintColor) {
//        _tintColor = AMRGB(0x286e9e, 1);
//        [self createGradient];
        self.backgroundColor = [UIColor colorWithPatternImage:kGetImage(@"leftnav-bg")];
        [self setNeedsDisplay];
    }
}

- (UIColor *)itemHighlightColor {
    return _itemHighlightColor ?: kAMDefaultItemHighlightColor;
}

- (void)setItemHighlightColor:(UIColor *)itemHighlightColor {
    if (itemHighlightColor != _itemHighlightColor) {
        _itemHighlightColor = itemHighlightColor;
        [self updateItemHighlight];
    }
}

- (void)setShowsItemHighlight:(BOOL)showsItemHighlight {
    if (showsItemHighlight != _showsItemHighlight) {
        _showsItemHighlight = showsItemHighlight;
        [self updateItemHighlight];
    }
}

////////////////////////////////////////////////////////////////////////
#pragma mark - Private
////////////////////////////////////////////////////////////////////////

- (CGFloat)dimensionToBeConsideredOfItem:(AMTabBarItem *)item {
    if (AMTabBarIsVertical(self.position)) {
        return item.frame.size.height;
    } else {
        return item.frame.size.width;  
    }
}

- (void)createGradient {
    if (_gradientRef != NULL) {
        CFRelease(_gradientRef);
    }
    
    UIColor *baseColor = self.tintColor;
    CGFloat hue, saturation, brightness, alpha;
    NSArray *colors = nil;
    
    // TODO: This is a temporary workaround because getHue:saturation:brightness:alpha: is iOS 5 and up
    // We need a better way of drawing a TabBar-Gradient
    if ([baseColor respondsToSelector:@selector(getHue:saturation:brightness:alpha:)]) {
        [baseColor getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
        
//        colors = [NSArray arrayWithObjects:
//                  [UIColor colorWithHue:hue saturation:saturation brightness:brightness+0.2 alpha:alpha],
//                  [UIColor colorWithHue:hue saturation:saturation brightness:brightness+0.15 alpha:alpha],
//                  [UIColor colorWithHue:hue saturation:saturation brightness:brightness+0.1 alpha:alpha],
//                  baseColor, nil];
        colors = @[AMRGB(0x286e9e, 1), AMRGB(0x41aba0, 1)];
    } else {
        colors = [NSArray arrayWithObjects:
                  baseColor,
                  baseColor, nil];
    }
    
    NSUInteger colorsCount = colors.count;
    CGColorSpaceRef colorSpace = CGColorGetColorSpace([[colors objectAtIndex:0] CGColor]);
    
    NSArray *locations = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],
                          [NSNumber numberWithFloat:0.3],
                          [NSNumber numberWithFloat:0.7],
                          [NSNumber numberWithFloat:1.0], nil];
    CGFloat *gradientLocations = NULL;
    NSUInteger locationsCount = locations.count;
    
    gradientLocations = (CGFloat *)malloc(sizeof(CGFloat) * locationsCount);
    
    for (NSUInteger i = 0; i < locationsCount; i++) {
        gradientLocations[i] = [[locations objectAtIndex:i] floatValue];
    }
    
    NSMutableArray *gradientColors = [[NSMutableArray alloc] initWithCapacity:colorsCount];
    [colors enumerateObjectsUsingBlock:^(id object, NSUInteger index, BOOL *stop) {
		[gradientColors addObject:(id)[(UIColor *)object CGColor]];
	}];
    CGFloat l[] = {0,0,1,0};
    _gradientRef = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, l);
    
    if (gradientLocations) {
        free(gradientLocations);
    }
}

- (void)updateItemHighlight {
    if (self.selectedItemIndex != NSNotFound) {
        CGRect itemRect = [[self.items objectAtIndex:self.selectedItemIndex] frame]; 
        
        if (_itemHighlightView == nil) {
            self.itemHighlightView = [[UIView alloc] initWithFrame:CGRectZero];
//            self.itemHighlightView.layer.cornerRadius = 5.f;
            [self addSubview:self.itemHighlightView];
        }
        
        self.itemHighlightView.backgroundColor = self.itemHighlightColor;
        self.itemHighlightView.frame = AMTabBarIsVertical(self.position) ? CGRectInset(itemRect, 0.f, 0.f) : CGRectInset(itemRect, 0.f, 0.f);
        self.itemHighlightView.hidden = !self.showsItemHighlight;
    } else {
        self.itemHighlightView.hidden = YES;
    }
}

@end
