//
//  UIViewController+NGTabBarItem.m
//  NGTabBarController
//
//  Created by Tretter Matthias on 24.04.12.
//  Copyright (c) 2012 NOUS Wissensmanagement GmbH. All rights reserved.
//

#import "UIViewController+AMTabBarItem.h"
#import "AMTabBarItem.h"
#import <objc/runtime.h>

static char itemKey;

@implementation UIViewController (AMTabBarItem)

- (void)ng_setTabBarItem:(AMTabBarItem *)ng_tabBarItem {
    objc_setAssociatedObject(self, &itemKey, ng_tabBarItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (AMTabBarItem *)ng_tabBarItem {
    return objc_getAssociatedObject(self, &itemKey);
}

@end
