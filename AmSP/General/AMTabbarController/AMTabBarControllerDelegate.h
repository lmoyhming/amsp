//
//  AMTabBarControllerDelegate.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "AMTabBarPosition.h"

@class AMTabBarController;
@class AMTabBarItem;

@protocol AMTabBarControllerDelegate <NSObject>

@required

/** Asks the delegate for the size of the given item */
- (CGSize)tabBarController:(AMTabBarController *)tabBarController
sizeOfItemForViewController:(UIViewController *)viewController
                   atIndex:(NSUInteger)index
                  position:(AMTabBarPosition)position;

@optional

/** Asks the delegate whether the specified view controller should be made active. */
- (BOOL)tabBarController:(AMTabBarController *)tabBarController
shouldSelectViewController:(UIViewController *)viewController
                 atIndex:(NSUInteger)index;

/** Tells the delegate that the user selected an item in the tab bar. */
- (void)tabBarController:(AMTabBarController *)tabBarController
 didSelectViewController:(UIViewController *)viewController
                 atIndex:(NSUInteger)index;

- (void)saveData;
- (void)sendMail;

@end
