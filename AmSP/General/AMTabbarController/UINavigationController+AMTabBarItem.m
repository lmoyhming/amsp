//
//  UINavigationController+AMTabBarItem.m
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "UINavigationController+AMTabBarItem.h"
#import "AMTabBarItem.h"
#import <objc/runtime.h>

static char itemKey;

@implementation UINavigationController (AMTabBarItem)

- (void)ng_setTabBarItem:(AMTabBarItem *)ng_tabBarItem {
    objc_setAssociatedObject(self, &itemKey, ng_tabBarItem, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (AMTabBarItem *)ng_tabBarItem {
    return objc_getAssociatedObject(self, &itemKey);
}

@end
