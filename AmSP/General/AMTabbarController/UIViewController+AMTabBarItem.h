//
//  UIViewController+NGTabBarItem.h
//  NGTabBarController
//
//  Created by Tretter Matthias on 24.04.12.
//  Copyright (c) 2012 NOUS Wissensmanagement GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AMTabBarItem;

@interface UIViewController (AMTabBarItem)

@property (nonatomic, strong, setter = ng_setTabBarItem:) AMTabBarItem *ng_tabBarItem;

@end
