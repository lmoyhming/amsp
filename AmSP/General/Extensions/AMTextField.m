//
//  AMTextField.m
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "AMTextField.h"


@implementation AMTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame textType:(NSInteger)textType{
    if (self == [super initWithFrame:frame]) {
        switch (textType) {
            case 1:
                self.text = @"0";
                break;
            case 2:
                self.text = @"¥0";
                break;
            case 3:
                self.text = @"0%";
                break;
            default:
                break;
        }
        self.keyboardType = UIKeyboardTypeNumberPad;
        self.textColor = AMRGB(0x0066a5, 1);
        [self addTarget:self action:@selector(beginEdit) forControlEvents:UIControlEventEditingDidBegin];
        [self addTarget:self action:@selector(endEdit) forControlEvents:UIControlEventEditingDidEnd];
    }
    return self;
}

- (void)awakeFromNib{
    [super awakeFromNib];
    switch (_textType) {
        case 1:
            self.text = @"0";
            break;
        case 2:
            self.text = @"¥0";
            break;
        case 3:
            self.text = @"0%";
            break;
        default:
            break;
    }
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.textColor = AMRGB(0x0066a5, 1);
    [self addTarget:self action:@selector(beginEdit) forControlEvents:UIControlEventEditingDidBegin];
    [self addTarget:self action:@selector(endEdit) forControlEvents:UIControlEventEditingDidEnd];
}

- (void)setTextType:(NSInteger)textType{
    _textType = textType;
    if (textType == 2) {
        self.text = @"¥0";
    }else if (textType == 3){
        self.text = @"0%";
    }else{
        self.text = @"0";
    }
}

//
- (void)layoutSubviews{
    CGSize size = [self.text boundingRectWithSize:CGSizeMake(999, self.frame.size.height)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{
                                                    NSFontAttributeName: self.font
                                                    }
                                          context:nil].size;
    CGFloat w = size.width + 50;
    self.frame = CGRectMake(self.center.x - w / 2, self.frame.origin.y, w, self.frame.size.height);
    [super layoutSubviews];
}

- (void)beginEdit{
    self.text = @"";
}

- (void)endEdit{
    if (self.text.length == 0) {
        self.text = @"0";
    }else{
//        [self.delegate textFieldDidEndEditing:self];
    }
}



@end
