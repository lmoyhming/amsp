//
//  AMChartView.h
//  AmSP
//
//  Created by Lee on 2017/3/14.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

typedef enum ChartDirectionType{
    ChartDirectionTypeH,
    ChartDirectionTypeV
} ChartDirectionType;

@class AMChartView;
@protocol AMChartDataSource <NSObject>

- (NSArray *)nameArrayInChartView:(AMChartView *)chartView;// y轴: X轴:
- (NSArray *)valueArrayForChartView:(AMChartView *)chartView;
- (NSArray *)colorArrayInChartView:(AMChartView *)chartView;
- (NSUInteger)axisLineSectionCountInChartView:(AMChartView *)chartView;
- (CGFloat)axisLineMaxValueInChartView:(AMChartView *)chartView;
- (CGFloat)axisLineStepInChartView:(AMChartView *)chartView;
- (UIEdgeInsets)chartViewPaddingInChartView:(AMChartView *)chartView;

@optional

- (CGFloat)chartBarHeightInChartView:(AMChartView *)chartView;
- (void)endEditValueWithChartView:(AMChartView *)chartView value:(CGFloat)value;

@end


#import <UIKit/UIKit.h>

@interface AMChartView : UIView

@property (nonatomic, assign) ChartDirectionType directionType;
@property (nonatomic, assign) BOOL showMark;
@property (nonatomic, assign) BOOL showTxt;
@property (nonatomic, assign) BOOL isMortality;
@property (nonatomic, assign) CGFloat topPadding;
@property (nonatomic, assign) CGFloat leftPadding;
@property (nonatomic, assign) CGFloat rightPadding;
@property (nonatomic, assign) CGFloat bottomPadding;

@property (nonatomic, assign) id<AMChartDataSource> datasource;

- (instancetype)initWithFrame:(CGRect)frame directionType:(ChartDirectionType)type chartName:(NSString *)name;

- (void)show;

@end
