//
//  AMTipView.m
//  AmSP
//
//  Created by Lee on 2017/3/16.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "AMTipView.h"

@interface AMTipView (){
    
}

@property (nonatomic, strong) NSString *content;

@end

@implementation AMTipView

- (instancetype)initWithContent:(NSString *)content{
    if (self == [super init]) {
        _content = content;
        [self configUI];
    }
    return self;
}

- (void)configUI{
    self.layer.cornerRadius = 6;
    self.layer.borderWidth = 1;
    self.layer.borderColor = AMRGB(0xa6a6a6, 1).CGColor;
    self.backgroundColor = AMRGB(0xf2f2f2, 1);
    
    UILabel *contentLabel = [[UILabel alloc] init];
    contentLabel.numberOfLines = 0;
    contentLabel.text = _content;
    contentLabel.textColor = [UIColor blackColor];
    [self addSubview:contentLabel];
    [contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(10, 10, 10, 10));
    }];
}

@end
