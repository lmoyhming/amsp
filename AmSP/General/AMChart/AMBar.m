//
//  AMBar.m
//  AmSP
//
//  Created by Lee on 2017/3/14.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "AMBar.h"

@implementation AMBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        backgroundLayer = [CAShapeLayer layer];
        [self.layer addSublayer:backgroundLayer];
        backgroundLayer.strokeColor = [UIColor grayColor].CGColor;
        backgroundLayer.frame = self.bounds;
        
        barLayer = [CAShapeLayer layer];
        [self.layer addSublayer:barLayer];
        barLayer.strokeColor = [UIColor greenColor].CGColor;
        barLayer.lineCap = kCALineCapButt;
        barLayer.frame = self.bounds;
        
        self.layer.borderWidth = 1;
        self.barWidth = self.bounds.size.width;
    }
    return self;
}

- (void)setBackground{
    backgroundPath = [UIBezierPath bezierPath];
    [backgroundPath moveToPoint:CGPointMake(self.bounds.origin.x, self.bounds.origin.y+self.bounds.origin.y+self.bounds.size.height/2)];
    [backgroundPath addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.origin.y+self.bounds.origin.y+self.bounds.size.height/2)];
    [backgroundPath setLineWidth:_barWidth];
    [backgroundPath setLineCapStyle:kCGLineCapSquare];
    backgroundLayer.path = backgroundPath.CGPath;
}

- (void)setProgress{
    barPath = [UIBezierPath bezierPath];
    [barPath moveToPoint:CGPointMake(self.bounds.origin.x, self.bounds.origin.y+self.bounds.origin.y+self.bounds.size.height/2)];
    [barPath addLineToPoint:CGPointMake(self.bounds.size.width*_barProgress, self.bounds.origin.y+self.bounds.origin.y+self.bounds.size.height/2)];
    [barPath setLineWidth:_barWidth];
    [barPath setLineCapStyle:kCGLineCapSquare];
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 1.0;
    pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pathAnimation.fromValue = @0.0f;
    pathAnimation.toValue = @1.0f;
    [barLayer addAnimation:pathAnimation forKey:nil];
    barLayer.strokeEnd = 1.0;
    barLayer.path = barPath.CGPath;
}

//设置柱子的宽度
- (void)setBarWidth:(CGFloat)progressWidth{
    _barWidth = progressWidth;
    backgroundLayer.lineWidth = _barWidth;
    barLayer.lineWidth = _barWidth;
    [self setBackground];
    [self setProgress];
}

- (void)setBackgroundColor:(UIColor *)backgroundColor{
    backgroundLayer.strokeColor = _barColor.CGColor;
}

- (void)setBarProgress:(CGFloat)barProgress{
    _barProgress = barProgress;
    [self setProgress];
}

- (void)setBarText:(NSString *)barText{
    textLayer = [CATextLayer layer];
    textLayer.string = barText;
    textLayer.foregroundColor = [[UIColor blackColor] CGColor];
    textLayer.fontSize = 16;
    textLayer.alignmentMode = kCAAlignmentLeft;
    textLayer.bounds = barLayer.bounds;
    textLayer.position = CGPointMake(self.bounds.size.width*3/2 + 5 , self.bounds.size.height/2);
    CABasicAnimation *fade = [self fadeAnimation];
    [textLayer addAnimation:fade forKey:nil];
    [self.layer addSublayer:textLayer];
}

//设置标题
- (void)setBarTittle:(NSString*)tittle{
    titleLayer = [CATextLayer layer];
    titleLayer.string = tittle;
    titleLayer.foregroundColor = [[UIColor blackColor] CGColor];
    titleLayer.fontSize = 16;
    titleLayer.alignmentMode = kCAAlignmentRight;
    titleLayer.bounds = barLayer.bounds;
    titleLayer.position = CGPointMake(-self.bounds.size.width/2 - 5 , self.bounds.size.height/2);
    CABasicAnimation *fade = [self fadeAnimation];
    [titleLayer addAnimation:fade forKey:nil];
    [self.layer addSublayer:titleLayer];
}
//渐变动画
- (CABasicAnimation*)fadeAnimation{
    CABasicAnimation* fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    fadeAnimation.toValue = [NSNumber numberWithFloat:1.0];
    fadeAnimation.duration = 2.0;
    return fadeAnimation;
}



@end
