//
//  AMChartView.m
//  AmSP
//
//  Created by Lee on 2017/3/14.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

//#define PADDING_BOTTOM  50
//#define PADDING_TOP     20
//#define PADDING_LEFT    214
//#define PADDING_RIGHT   185
#define FLOAT_SPLIT     10
#define WIDTH_LINE_AXIS 1

#import "AMChartView.h"
#import "AMBar.h"
#import "AMTipView.h"
#import "AMChartMarkView.h"

@interface AMChartView () <UITextFieldDelegate>{
    CGPoint originPoint;
    CGFloat xWidth;
    CGFloat yHeight;
    CGFloat viewW;
    CGFloat viewH;
    CGFloat offsetY;
    CGFloat offsetX;
    
    CGContextRef ctx;
    
    UIButton *selectedBar;
    AMChartMarkView *markView;
}

@property (nonatomic, strong) NSArray *nameArray;
@property (nonatomic, strong) NSArray *valueArray;
@property (nonatomic, strong) NSArray *colorArray;
@property (nonatomic, assign) NSUInteger section;
@property (nonatomic, assign) CGFloat maxValue;
@property (nonatomic, assign) CGFloat step;
@property (nonatomic, assign) CGFloat barHeight;
@property (nonatomic, assign) NSString *chartTitle;

@end

@implementation AMChartView

- (void)drawRect:(CGRect)rect {
    ctx = UIGraphicsGetCurrentContext();
    
    viewW = self.bounds.size.width;
    viewH = self.bounds.size.height;
    
    originPoint = CGPointMake(_leftPadding, viewH - _bottomPadding);
    yHeight = originPoint.y - _topPadding;
    xWidth = viewW - originPoint.x - _rightPadding;
    
    CGContextSetLineCap(ctx, kCGLineCapRound);
    CGContextSetLineWidth(ctx, WIDTH_LINE_AXIS);
    CGContextSetAllowsAntialiasing(ctx, true);
    CGContextSetRGBStrokeColor(ctx, 0, 0, 0, 1); // line color
    CGContextBeginPath(ctx);
    
    // draw x
    CGContextMoveToPoint(ctx, originPoint.x, originPoint.y);
    CGContextAddLineToPoint(ctx, viewW - _rightPadding / 2, originPoint.y);
    
    // draw y
    CGContextMoveToPoint(ctx, originPoint.x, _topPadding);
    CGContextAddLineToPoint(ctx, originPoint.x, originPoint.y);
    
    CGContextStrokePath(ctx);
    
    // draw title
    [[UIColor redColor] setStroke];
    NSString *label = _chartTitle;
    CGSize labelSize = [label boundingRectWithSize:self.bounds.size
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{
                                                     NSFontAttributeName: AMFONT(14),
                                                     NSForegroundColorAttributeName: AMRGB(0x797979, 1)
                                                     }
                                           context:nil].size;
    [label drawInRect:CGRectMake(originPoint.x - 30 - labelSize.width, originPoint.y + 20, labelSize.width, labelSize.height)
       withAttributes:@{
                        NSFontAttributeName: AMFONT(14),
                        NSForegroundColorAttributeName: AMRGB(0x797979, 1)
                        }];
    
    
    CGContextStrokePath(ctx);
    
    offsetX = xWidth / (_directionType == ChartDirectionTypeH ? _section : _nameArray.count);
    offsetY = yHeight / (_directionType == ChartDirectionTypeH ? _nameArray.count : _section);
    [self drawSectionX];
    [self drawSectionY];
    
    
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    if (_directionType == ChartDirectionTypeH) {
        // draw x label
        for (int i = 0; i < _section+1; i++) {
            NSInteger value = i * _step;
            style.alignment = NSTextAlignmentCenter;
            NSString *valueString = [NSString stringWithFormat:@"%@", [Global getMoneyStringWithMoneyNumber:value decimal:NO]];
            CGSize stringSize = [valueString boundingRectWithSize:self.bounds.size
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{
                                                                    NSFontAttributeName: AMFONT(16)
                                                                    }
                                                          context:nil].size;
            [valueString drawInRect:CGRectMake(originPoint.x + (i * offsetX) - stringSize.width / 2, originPoint.y + FLOAT_SPLIT*2, stringSize.width, stringSize.height)
                     withAttributes:@{
                                      NSFontAttributeName: AMFONT(16),
                                      NSForegroundColorAttributeName: AMRGB(0x797979, 1)
                                      }];
        }
        
        // draw y label
        for (int i = 0; i < _nameArray.count; i++) {
            NSString *nameString = _nameArray[i];
            
            CGSize stringSize = [nameString boundingRectWithSize:self.bounds.size
                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                      attributes:@{
                                                                   NSFontAttributeName: AMFONT(14),
                                                                   NSForegroundColorAttributeName: AMRGB(0x0066a5, 1),
                                                                   NSParagraphStyleAttributeName: style
                                                                   }
                                                         context:nil].size;
            [[UIColor redColor] setStroke];
            
            style.alignment = NSTextAlignmentRight;
            [nameString drawInRect:CGRectMake(originPoint.x - stringSize.width - 15, (originPoint.y - yHeight * ((i*2.0f+1.0f)/(_nameArray.count * 2)) - stringSize.height / 2), stringSize.width, stringSize.height)
                    withAttributes:@{
                                     NSFontAttributeName: AMFONT(14),
                                     NSForegroundColorAttributeName: AMRGB(0x0066a5, 1),
                                     NSParagraphStyleAttributeName: style
                                     }];
        }
    }else{
        // draw x label
        CGFloat vOffsetX = xWidth / _nameArray.count;
        CGFloat vOffsetY = yHeight / _section;
        for (int i = 0; i < _nameArray.count; i++) {
//            NSInteger value = i * _step;
            NSString *nameString = _nameArray[i];
            style.alignment = NSTextAlignmentCenter;
            CGSize stringSize = [nameString boundingRectWithSize:CGSizeMake(vOffsetX, self.bounds.size.height)
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{
                                                                    NSFontAttributeName: AMFONT(12),
                                                                    NSParagraphStyleAttributeName: style
                                                                    }
                                                          context:nil].size;
            [nameString drawInRect:CGRectMake(originPoint.x + vOffsetX / 2 + (i * vOffsetX) - vOffsetX / 2, originPoint.y + FLOAT_SPLIT*2, vOffsetX, stringSize.height)
                     withAttributes:@{
                                      NSFontAttributeName: AMFONT(12),
                                      NSForegroundColorAttributeName: AMRGB(0x797979, 1),
                                      NSParagraphStyleAttributeName: style
                                      }];
        }
        
        // draw y label
        for (int i = 0; i < _section + 1; i++) {
            if (_isMortality) {
                CGFloat value = i * _step;
                NSString *valueString = [NSString stringWithFormat:@"%.1f%%", value + 3.4];
                NSMutableParagraphStyle *style = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
                CGSize stringSize = [valueString boundingRectWithSize:self.bounds.size
                                                              options:NSStringDrawingUsesLineFragmentOrigin
                                                           attributes:@{
                                                                        NSFontAttributeName: AMFONT(14),
                                                                        NSForegroundColorAttributeName: AMRGB(0x0066a5, 1),
                                                                        NSParagraphStyleAttributeName: style
                                                                        }
                                                              context:nil].size;
                [[UIColor redColor] setStroke];
                style.alignment = NSTextAlignmentRight;
                [valueString drawInRect:CGRectMake(originPoint.x - stringSize.width - 15, (originPoint.y - vOffsetY * i - stringSize.height / 2), stringSize.width, stringSize.height)
                         withAttributes:@{
                                          NSFontAttributeName: AMFONT(14),
                                          NSForegroundColorAttributeName: AMRGB(0x0066a5, 1),
                                          NSParagraphStyleAttributeName: style
                                          }];
            }else{
                NSInteger value = i * _step;
                NSString *valueString = [NSString stringWithFormat:@"%ld%%", value];
                NSMutableParagraphStyle *style = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
                CGSize stringSize = [valueString boundingRectWithSize:self.bounds.size
                                                              options:NSStringDrawingUsesLineFragmentOrigin
                                                           attributes:@{
                                                                        NSFontAttributeName: AMFONT(14),
                                                                        NSForegroundColorAttributeName: AMRGB(0x0066a5, 1),
                                                                        NSParagraphStyleAttributeName: style
                                                                        }
                                                              context:nil].size;
                [[UIColor redColor] setStroke];
                style.alignment = NSTextAlignmentRight;
                [valueString drawInRect:CGRectMake(originPoint.x - stringSize.width - 15, (originPoint.y - vOffsetY * i - stringSize.height / 2), stringSize.width, stringSize.height)
                         withAttributes:@{
                                          NSFontAttributeName: AMFONT(14),
                                          NSForegroundColorAttributeName: AMRGB(0x0066a5, 1),
                                          NSParagraphStyleAttributeName: style
                                          }];
            }
            
        }
    }
    
    
    // change mark view frame
    [markView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(80, 45));
        make.top.mas_equalTo(self).offset(_topPadding);
    }];
    
    // draw bar
    [self removeAllBar];
    for (int i = 0; i < _valueArray.count; i++) {
        id subObj = _valueArray[i];
        CGRect frame;
        if (_directionType == ChartDirectionTypeH) {
            frame = CGRectMake(originPoint.x, originPoint.y - yHeight * ((i*2.0f+1.0f)/(_nameArray.count * 2)) - 10, 0, _barHeight);
        }else{
            frame = CGRectMake(originPoint.x + (xWidth / _nameArray.count) / 2 + (i * (xWidth / _nameArray.count)) - _barHeight / 2, originPoint.y, _barHeight, 0);
            CGFloat textWidth = 0;
            if ([subObj isKindOfClass:[NSArray class]]) {
                textWidth = _barHeight * 2;
            }else{
                textWidth = _barHeight;
            }
            
        }
        
        if ([subObj isKindOfClass:[NSArray class]]) {
            
            NSArray *subArray = [NSArray arrayWithArray:(NSArray *)subObj];
            for (int j = 0; j < subArray.count; j++) {
                UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
                btn.tag = i*2+j + 1;
                
                [btn addTarget:self action:@selector(showTipView:) forControlEvents:UIControlEventTouchUpInside];
                if (j == 0) {
                    if (_directionType == ChartDirectionTypeV) {
                        btn.frame = CGRectMake(frame.origin.x - _barHeight / 2, frame.origin.y, frame.size.width, frame.size.height);
                    }else{
                        btn.frame = CGRectMake(frame.origin.x, frame.origin.y + _barHeight / 2, frame.size.width, frame.size.height);
                    }
                    [btn setBackgroundColor:AMRGB(0xb5b5b5, 1)];
                }else{
                    
                    if (_directionType == ChartDirectionTypeV) {
                        btn.frame = CGRectMake(frame.origin.x + _barHeight / 2, frame.origin.y, frame.size.width, frame.size.height);
                    }else{
                        btn.frame = CGRectMake(frame.origin.x, frame.origin.y - _barHeight / 2, frame.size.width, frame.size.height);
                    }
                    [btn setBackgroundColor:AMRGB(0x789fdc, 1)];
                }
                if (_showTxt) {
                    [self configTxtView:CGRectMake(btn.frame.origin.x, originPoint.y, _barHeight, 30) index:btn.tag value:subObj[j]];
                }
                
                [self addSubview:btn];
//                __block CGFloat value = [subArray[j] integerValue] * xWidth / _maxValue;
                [UIView animateWithDuration:1
                                 animations:^{
                                     CGFloat value = [subArray[j] integerValue] * xWidth / _maxValue;
//                                     if (value > xWidth) {
//                                         value = viewW -  xWidth;
//                                         [btn setBackgroundColor:[UIColor redColor]];
//                                     }
                                     if (_directionType == ChartDirectionTypeH) {
                                         btn.frame = CGRectMake(frame.origin.x, btn.frame.origin.y, value, _barHeight);
                                     }else{
                                         value = [subArray[j] integerValue] * yHeight / _maxValue;
                                         btn.frame = CGRectMake(btn.frame.origin.x, btn.frame.origin.y - value, _barHeight, value);
                                     }
                                 }];
            }
        }else{
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.tag = (i + 1) * 10;
            
            __block CGFloat value = 0;
            if (_directionType == ChartDirectionTypeH) {
                
                value = [_valueArray[i] floatValue] * xWidth / _maxValue;
                if (value > xWidth) {
                    value = xWidth;
                }
            }else{
                value = ([_valueArray[i] floatValue] - 3.4) * yHeight / (_maxValue - 3.4);
                if (value > yHeight) {
                    value = yHeight;
                }
            }
            if (value < 0) {
                value = 0;
            }
            NSLog(@"%f", value);
            btn.frame = frame;
            if (_colorArray.count == _valueArray.count) {
                [btn setBackgroundColor:_colorArray[i]];
            }else{
                [btn setBackgroundColor:[UIColor blackColor]];
            }
            
            [btn addTarget:self action:@selector(showTipView:) forControlEvents:UIControlEventTouchUpInside];
            
            if (_showTxt) {
                [self configTxtView:CGRectMake(btn.frame.origin.x, originPoint.y, _barHeight, 30) index:btn.tag value:subObj];
            }
            [self addSubview:btn];
            
            [UIView animateWithDuration:1
                             animations:^{
                                 
                                 if (_directionType == ChartDirectionTypeH) {
                                     btn.frame = CGRectMake(frame.origin.x, btn.frame.origin.y, value, _barHeight);
                                 }else{
                                     btn.frame = CGRectMake(btn.frame.origin.x, btn.frame.origin.y - value, _barHeight, value);
                                 }
                             }];
        }
    }
}

- (void)drawSectionX{
    for (int i = 0; i < _section + 1; i++) {
        CGFloat x = originPoint.x;
        [[UIColor blackColor] setStroke];
        CGContextMoveToPoint(ctx, x + (i * offsetX), originPoint.y);
        if (_directionType == ChartDirectionTypeH) {
            CGContextAddLineToPoint(ctx, x + (i * offsetX), originPoint.y+FLOAT_SPLIT);
        }else{
            CGContextAddLineToPoint(ctx, originPoint.x + (xWidth / _nameArray.count * i), originPoint.y + FLOAT_SPLIT);
        }
        CGContextStrokePath(ctx);
        
        [[UIColor blackColor] setStroke];
        CGContextMoveToPoint(ctx, x + (i * offsetX), originPoint.y);
        if (_directionType == ChartDirectionTypeH && i > 0) {
            CGContextAddLineToPoint(ctx, x + (i * offsetX), originPoint.y-yHeight);
        }
        CGContextStrokePath(ctx);
    }
}

- (void)drawSectionY{
    
    NSInteger count = _directionType == ChartDirectionTypeH ? _nameArray.count : _section;
    [[UIColor blackColor] setStroke];
    for (int i = 0; i < count + 1 ; i++) {
        CGFloat y = originPoint.y;
        
        CGContextMoveToPoint(ctx, originPoint.x, y - (i * offsetY));
        if (_directionType == ChartDirectionTypeH) {
            CGContextAddLineToPoint(ctx, originPoint.x - FLOAT_SPLIT, y - (i * offsetY));
        }else{
            CGContextAddLineToPoint(ctx, originPoint.x - FLOAT_SPLIT, y - (yHeight / _section * i));
        }
    }
    CGContextStrokePath(ctx);
}

- (instancetype)initWithFrame:(CGRect)frame directionType:(ChartDirectionType)type chartName:(NSString *)name{
    if (self == [super initWithFrame:frame]) {
        _directionType = type;
        _barHeight = 20;
        _chartTitle = name;
        [self configMarkView];
    }
    return self;
}

- (void)configMarkView{
    if (!markView) {
        markView = [[AMChartMarkView alloc] init];
        markView.hidden = YES;
        [self addSubview:markView];
        [markView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self).offset(-20);
            make.top.mas_equalTo(self);
        }];
    }
}

- (void)show{
    if ([self.datasource respondsToSelector:@selector(nameArrayInChartView:)]) {
        self.nameArray = [NSArray arrayWithArray:[self.datasource nameArrayInChartView:self]];
    }
    if ([self.datasource respondsToSelector:@selector(valueArrayForChartView:)]) {
        self.valueArray = [NSArray arrayWithArray:[self.datasource valueArrayForChartView:self]];
    }
    if ([self.datasource respondsToSelector:@selector(axisLineSectionCountInChartView:)]) {
        self.section = [self.datasource axisLineSectionCountInChartView:self];
    }
    if ([self.datasource respondsToSelector:@selector(colorArrayInChartView:)]) {
        self.colorArray = [NSArray arrayWithArray:[self.datasource colorArrayInChartView:self]];
    }
    if ([self.datasource respondsToSelector:@selector(axisLineMaxValueInChartView:)]) {
        self.maxValue = [self.datasource axisLineMaxValueInChartView:self];
    }
    if ([self.datasource respondsToSelector:@selector(axisLineStepInChartView:)]) {
        self.step = [self.datasource axisLineStepInChartView:self];
    }
    if ([self.datasource respondsToSelector:@selector(chartBarHeightInChartView:)]) {
        self.barHeight = [self.datasource chartBarHeightInChartView:self];
    }
    if ([self.datasource respondsToSelector:@selector(chartViewPaddingInChartView:)]) {
        UIEdgeInsets inset = [self.datasource chartViewPaddingInChartView:self];
        _topPadding = inset.top;
        _leftPadding = inset.left;
        _rightPadding = inset.right;
        _bottomPadding = inset.bottom;
    }
    [self setNeedsDisplay];
}

- (void)removeAllBar{
    for (UIButton *btn in self.subviews) {
        if ([btn isKindOfClass:[UIButton class]]) {
            [btn removeFromSuperview];
        }
    }
}

- (void)showTipView:(UIButton *)sender{
    return;
    AMTipView *tip = [self viewWithTag:100];
    if (tip) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             tip.alpha = 0;
                         }
                         completion:^(BOOL finished) {
                             [tip removeFromSuperview];
                         }];
    }
    if (sender == selectedBar && tip) {
        return;
    }
    selectedBar = sender;
    AMTipView *tipView = [[AMTipView alloc] initWithContent:@"¥24,8000"];
    tipView.alpha = 0;
    tipView.tag = 100;
    [self addSubview:tipView];
    [tipView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(sender.mas_right);
        make.bottom.mas_equalTo(sender.mas_top);
    }];
    [UIView animateWithDuration:0.3
                     animations:^{
                         tipView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

#pragma mark -
- (void)setShowMark:(BOOL)showMark{
    _showMark = showMark;
    markView.hidden = !showMark;
}

- (void)configTxtView:(CGRect)frame index:(NSInteger)index value:(NSNumber *)value{
    AMTextField *textField = [self viewWithTag:index];
    if (!textField) {
        UIView *bgView = [[UIView alloc] init];
        
        bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon-textBg"]];
        [self addSubview:bgView];
        [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(frame.origin.x);
            make.top.mas_equalTo(5);
            make.height.mas_equalTo(30);
            make.width.mas_equalTo(_barHeight);
        }];
        AMTextField *txt = [[AMTextField alloc] initWithFrame:CGRectZero textType:3];
        txt.delegate = self;
        txt.text = [[value stringValue] rangeOfString:@"%"].location == NSNotFound ? [[value stringValue] stringByAppendingString:@"%"] : [value stringValue];
        txt.tag = index;
        [bgView addSubview:txt];
        txt.textAlignment = NSTextAlignmentCenter;
        [txt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(bgView);
        }];
    }
    textField.text = [[value stringValue] rangeOfString:@"%"].location == NSNotFound ? [[value stringValue] stringByAppendingString:@"%"] : [value stringValue];
    
    
}

#pragma mark - text field delegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (_datasource && [_datasource respondsToSelector:@selector(endEditValueWithChartView:value:)]) {
        [_datasource endEditValueWithChartView:self value:[textField.text floatValue]];
    }
}

@end
