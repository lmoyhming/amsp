//
//  AMChartMarkView.m
//  AmSP
//
//  Created by Lee on 2017/3/24.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "AMChartMarkView.h"

@implementation AMChartMarkView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    if (self == [super init]) {
        [self configUI];
    }
    return self;
}

- (void)configUI{
    UIView *markTopView = [[UIView alloc] init];
    [self addSubview:markTopView];
    //0x558ed5
    markTopView.backgroundColor = AMRGB(0x558ed5, 1);
    [markTopView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self).offset(-8);
        make.size.mas_equalTo(CGSizeMake(13, 13));
        make.left.mas_equalTo(5);
    }];
    UILabel *topLabel = [[UILabel alloc] init];
    topLabel.textColor = AMRGB(0x558ed5, 1);
    topLabel.text = @"After";
    topLabel.font = AMFONT(14);
    [self addSubview:topLabel];
    [topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(markTopView);
        make.left.mas_equalTo(markTopView.mas_right).offset(8);
    }];
    
    UIView *markBottomView = [[UIView alloc] init];
    [self addSubview:markBottomView];
    //0xa6a6a6
    markBottomView.backgroundColor = AMRGB(0xa6a6a6, 1);
    [markBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self).offset(8);
        make.size.mas_equalTo(CGSizeMake(13, 13));
        make.left.mas_equalTo(5);
    }];
    UILabel *bottomLabel = [[UILabel alloc] init];
    bottomLabel.textColor = AMRGB(0xa6a6a6, 1);
    bottomLabel.text = @"Before";
    bottomLabel.font = AMFONT(14);
    [self addSubview:bottomLabel];
    [bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(markBottomView);
        make.left.mas_equalTo(markBottomView.mas_right).offset(8);
    }];
}

@end
