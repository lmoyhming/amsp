//
//  AMBar.h
//  AmSP
//
//  Created by Lee on 2017/3/14.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface AMBar : UIView {
    CAShapeLayer *backgroundLayer;
    UIBezierPath *backgroundPath;
    CAShapeLayer *barLayer;
    UIBezierPath *barPath;
    CATextLayer *textLayer;
    CATextLayer *titleLayer;
}

@property (nonatomic) UIColor *backgroundColor;
@property (nonatomic) UIColor *barColor;
@property (nonatomic) CGFloat barProgress;
@property (nonatomic) CGFloat barWidth;
@property (nonatomic) NSString *barText;
@property (nonatomic) NSString *barTitle;

@end
