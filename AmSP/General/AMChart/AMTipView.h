//
//  AMTipView.h
//  AmSP
//
//  Created by Lee on 2017/3/16.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AMTipView : UIView

- (instancetype)initWithContent:(NSString *)content;

@end
