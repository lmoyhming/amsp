//
//  BaseViewController.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AMTabBarController.h"

@interface BaseViewController : UIViewController

@property (nonatomic, strong) AMTabBarController *myTabBarController;
@property (nonatomic, assign) BOOL isHomeStyle;

@property (nonatomic, strong) UIView *bottomView;

@end
