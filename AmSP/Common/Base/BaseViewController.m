//
//  BaseViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "BaseViewController.h"
#import "GuideViewController.h"
#import "HomeViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configBottomUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([self isKindOfClass:[GuideViewController class]]) {
        self.view.backgroundColor = AMClearColor;
    }else{
        self.view.backgroundColor = AMRGB(0xf6f6f6, 1);
    }
}

- (void)configBottomUI{
    _bottomView = [[UIView alloc] init];
    _bottomView.backgroundColor = [UIColor whiteColor];
    AMViewShadow(_bottomView, CGSizeMake(0, -2));
    
    [self.view addSubview:_bottomView];
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(kBottomHeight);
    }];
    
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:kGetImage(@"logo")];
    [self.view addSubview:logoImageView];
    [logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(_bottomView);
        make.right.mas_equalTo(_bottomView).offset(-20);
    }];
    
    if ([self isKindOfClass:[GuideViewController class]]) {
        _bottomView.alpha = 0.3;
        NSString *str = @"Disclaimer: Need disclaimer language. Eg. \"This calculator demonstrates the behavior of the NNV and other common \ncost-effectiveness metrics under various assumptions of a hypothetical disease and vaccine. It is not intended to approximate the \nclinical or economic value of any rpoduct, either existing or under development\"";
        UILabel *bottomLabel = [[UILabel alloc] init];
        bottomLabel.font = AMFONT(12);
        bottomLabel.numberOfLines = 0;
        bottomLabel.text = str;
        [self.view addSubview:bottomLabel];
        [bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.mas_equalTo(_bottomView);
            make.left.mas_equalTo(20);
            make.right.mas_equalTo(-20);
        }];
    }else{
        _bottomView.alpha = 1.0f;
        NSString *str1 = @"ECONOMIC EVALUATION OF IMPLEMENTING HOSPITAL";
        NSString *str2 = @"ANTIMICROBIAL STEWARDSHIP PROGRAMS (AmSP)";
        NSString *str3 = @"IN CHINA";
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@ %@", str1,str2,str3]];
        [attrString setAttributes:@{
                                    NSFontAttributeName: AMFONT(18),
                                    NSForegroundColorAttributeName: AMRGB(0x2b759e, 1),
                                    }
                            range:NSMakeRange(0, str1.length)];
        [attrString setAttributes:@{
                                    NSFontAttributeName: AMFONT(18),
                                    NSForegroundColorAttributeName: AMRGB(0x40a8a0, 1)
                                    }
                            range:NSMakeRange(str1.length + 1, str2.length)];
        [attrString setAttributes:@{
                                    NSFontAttributeName: AMFONT(18),
                                    NSForegroundColorAttributeName: AMRGB(0x2b759e, 1)
                                    }
                            range:NSMakeRange(str1.length + str2.length + 2, str3.length)];
        
        UILabel *bottomLabel = [[UILabel alloc] init];
        bottomLabel.numberOfLines = 0;
        bottomLabel.textAlignment = NSTextAlignmentRight;
        bottomLabel.attributedText = attrString;
        [self.view addSubview:bottomLabel];
        [bottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(logoImageView.mas_left).offset(-20);
            make.top.bottom.mas_equalTo(_bottomView);
        }];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (AMTabBarController *)myTabBarController{
    return (AMTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
