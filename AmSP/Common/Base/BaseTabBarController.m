//
//  BaseTabBarController.m
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "BaseTabBarController.h"

@interface BaseTabBarController ()

- (void)setupForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

@end

@implementation BaseTabBarController

- (instancetype)initWithDelegate:(id<AMTabBarControllerDelegate>)delegate{
    self = [super initWithDelegate:delegate];
    if (self) {
        self.animation = AMTabBarControllerAnimationFade;
        self.tabBar.tintColor = [UIColor blueColor];
        self.tabBar.itemPadding = 10.0f;
        self.view.backgroundColor = [UIColor colorWithPatternImage:kGetImage(@"bg")];
        [self setupForInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation];
    }
    return self;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self setupForInterfaceOrientation:toInterfaceOrientation];
}

////////////////////////////////////////////////////////////////////////
#pragma mark - Private
////////////////////////////////////////////////////////////////////////

- (void)setupForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation; {
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        self.tabBarPosition = AMTabBarPositionBottom;
        self.tabBar.showsItemHighlight = NO;
        self.tabBar.layoutStrategy = AMTabBarLayoutStrategyCentered;
    } else {
        self.tabBarPosition = AMTabBarPositionLeft;
        self.tabBar.showsItemHighlight = YES;
        self.tabBar.layoutStrategy = AMTabBarLayoutStrategyStrungTogether;
    }
}

@end
