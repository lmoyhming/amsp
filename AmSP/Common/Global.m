//
//  Global.m
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "Global.h"

@implementation Global

+ (NSString *)getMoneyStringWithMoneyNumber:(double)money decimal:(BOOL)decimal{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    // 设置格式
    
    if (decimal) {
        numberFormatter.positivePrefix = @"¥";
        [numberFormatter setPositiveFormat:@"###,##0;"];
        numberFormatter.minimumFractionDigits = 2;
        NSString *formattedNumberString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:money]];
        return formattedNumberString;
    }else{
        [numberFormatter setPositiveFormat:@"###,###;"];
        NSString *formattedNumberString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:(NSInteger)money]];
        return [NSString stringWithFormat:@"¥%@", formattedNumberString];
    }
    
    NSNumber *num1 = [NSNumber numberWithDouble:1234567.8369];
    
    // ==================== 类方法 ====================
    
    // 四舍五入的整数
    NSString *numberNoStyleStr                 = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterNoStyle];
    
    // 小数形式
    NSString *numberDecimalStyleStr            = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterDecimalStyle];
    
    // 货币形式 -- 本地化
    NSString *numberCurrencyStyleStr           = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterCurrencyStyle];
    
    // 百分数形式
    NSString *numberPercentStyleStr            = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterPercentStyle];
    
    // 科学计数
    NSString *numberScientificStyleStr         = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterScientificStyle];
    
    // 朗读形式 -- 本地化
    NSString *numberSpellOutStyleStr           = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterSpellOutStyle];
    
    // 序数形式 -- 本地化
    NSString *numberOrdinalStyleStr            = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterOrdinalStyle];
    
    // 货币形式 ISO -- 本地化
    NSString *numberCurrencyISOStyleStr        = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterCurrencyISOCodeStyle];
    
    // 货币形式 -- 本地化
    NSString *numberCurrencyPluralStyleStr     = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterCurrencyPluralStyle];
    
    // 会计计数 -- 本地化
    NSString *numberCurrencyAccountingStyleStr = [NSNumberFormatter localizedStringFromNumber:num1 numberStyle:NSNumberFormatterCurrencyAccountingStyle];
    
    NSLog(@"No Style                  = %@",numberNoStyleStr);                // No Style                  = 1234568
    NSLog(@"Decimal Style             = %@",numberDecimalStyleStr);           // Decimal Style             = 1,234,567.837
    NSLog(@"Currency Style            = %@",numberCurrencyStyleStr);          // Currency Style            = $1,234,567.84
    NSLog(@"Percent Style             = %@",numberPercentStyleStr);           // Percent Style             = 123,456,784%
    NSLog(@"Scientific Style          = %@",numberScientificStyleStr);        // Scientific Style          = 1.2345678369E6
    // Spell Out Style           = one million two hundred thirty-four thousand five hundred sixty-seven point eight three six nine
    NSLog(@"Spell Out Style           = %@",numberSpellOutStyleStr);
    NSLog(@"Ordinal Style             = %@",numberOrdinalStyleStr);            // Ordinal Style             = 1,234,568th
    NSLog(@"Currency ISO Style        = %@",numberCurrencyISOStyleStr);        // Currency ISO Style        = USD1,234,567.84
    NSLog(@"Currency plural Style     = %@",numberCurrencyPluralStyleStr);     // Currency plural Style     = 1,234,567.84 US dollars
    NSLog(@"Currency accounting Style = %@",numberCurrencyAccountingStyleStr); // Currency accounting Style = $1,234,567.84
    
    // ==================== 定制 ====================
    
    
    
}

+ (NSString *)getPercentStringWithPercentNumber:(NSInteger)number{
    // 设置格式
    return [NSNumberFormatter localizedStringFromNumber:@(number) numberStyle:NSNumberFormatterPercentStyle];;
}

@end
