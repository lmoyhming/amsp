//
//  Global.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Global : NSObject

+ (NSString *)getMoneyStringWithMoneyNumber:(double)money decimal:(BOOL)decimal;
+ (NSString *)getPercentStringWithPercentNumber:(NSInteger)money;

@end
