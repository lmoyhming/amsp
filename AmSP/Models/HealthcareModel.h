//
//  HealthcareModel.h
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface HealthcareModel : JSONModel

@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSNumber *overallNumber;
@property (nonatomic, copy) NSNumber *before;
@property (nonatomic, copy) NSNumber *after;
@property (nonatomic, copy) NSNumber *cost;

@end
