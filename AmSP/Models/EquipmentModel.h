//
//  EquipmentModel.h
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface EquipmentModel : JSONModel

@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSNumber *sixtyNumber;
@property (nonatomic, copy) NSNumber *eightyNumber;
@property (nonatomic, copy) NSNumber *cost;

@end
