//
//  StaffModel.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface StaffModel : JSONModel

@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSNumber *number;
@property (nonatomic, copy) NSNumber *hours;
@property (nonatomic, copy) NSNumber *cost;

@end
