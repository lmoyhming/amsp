//
//  DrugsModel.h
//  AmSP
//
//  Created by Lee on 2017/4/7.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface DrugsModel : JSONModel

@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSNumber *before;
@property (nonatomic, copy) NSNumber *after;

@end
