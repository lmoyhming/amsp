//
//  AppMacro.h
//  AmSP
//
//  Created by Lee on 2017/3/10.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#ifndef AppMacro_h
#define AppMacro_h

#define USER_DEFAULT    [NSUserDefaults standardUserDefaults]
#define SHARE_APP       ((AppDelegate *)[[UIApplication sharedApplication] delegate])

#define KEY_WINDOW      [UIApplication sharedApplication].keyWindow
#define SCREEN_BOUNDS   [UIScreen mainScreen].bounds
#define SCREEN_WIDTH    [UIScreen mainScreen].bounds.size.width
#define SCREENH_HEIGHT  [UIScreen mainScreen].bounds.size.height

#define classFromSB(name)    [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:name]

#define AMCOLOR(r,g,b)  [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]
#define AMSCOLOR(c)     [UIColor colorWithRed:(c)/255.0 green:(c)/255.0 blue:(c)/255.0 alpha:1.0]
#define AMClearColor    [UIColor clearColor]

#define AMRGB(hexValue,a) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16)) / 255.0 green:((float)((hexValue & 0xFF00) >> 8)) / 255.0 blue:((float)(hexValue & 0xFF)) / 255.0 alpha:1.0f]

#define AMFONT(f)       [UIFont fontWithName:@"CenturyGothic" size:f]
#define AMFONT_B(f)     [UIFont fontWithName:@"CenturyGothic-Bold" size:f]

#define AMWeakSelf(type) __weak typeof(type) weak##type = type;
#define AMStrongSelf(type)  __strong typeof(type) type = weak##type;

#define AMViewBorderRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]]

#define AMViewShadow(View,Size)\
\
View.layer.shadowOffset = Size;\
View.layer.shadowColor = [UIColor lightGrayColor].CGColor;\
View.layer.shadowOpacity = 0.5;\
View.layer.shadowRadius = 2;\
View.clipsToBounds = NO;

#define kGetImage(imageName) [UIImage imageNamed:[NSString stringWithFormat:@"%@",imageName]]

#define kBottomHeight   88.0f



#endif /* AppMacro_h */
