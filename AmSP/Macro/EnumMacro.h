//
//  EnumMacro.h
//  AmSP
//
//  Created by Lee on 2017/3/10.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#ifndef EnumMacro_h
#define EnumMacro_h

typedef enum AMTextFieldType{
    AMTextFieldTypeNormal,
    AMTextFieldTypeMoney,
    AMTextFieldTypePercent
} AMTextFieldType;


#endif /* EnumMacro_h */
