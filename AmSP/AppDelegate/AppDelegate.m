//
//  AppDelegate.m
//  AmSP
//
//  Created by Lee on 2017/3/9.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "AppDelegate.h"
#import "AMTabBarController.h"

#import "BaseTabBarController.h"
#import "StaffViewController.h"
#import "GuideViewController.h"
#import "HomeViewController.h"
#import "EquipmentViewController.h"
#import "HealthcareViewController.h"
#import "DrugsViewController.h"
#import "ClinicalViewController.h"
#import "ReturnViewController.h"

@interface AppDelegate () <AMTabBarControllerDelegate>{
    AMTabBarController *tabbar;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    GuideViewController *vc1 = classFromSB(@"sbGuide");
    StaffViewController *vc2 = classFromSB(@"sbStaff");
    EquipmentViewController *vc3 = classFromSB(@"sbEquipment");
    HealthcareViewController *vc4 = classFromSB(@"sbHealth");
    DrugsViewController *vc5 = classFromSB(@"sbDrugs");
    ClinicalViewController *vc6 = classFromSB(@"sbClinical");
    ReturnViewController *vc7 = classFromSB(@"sbReturn");
    
    vc1.ng_tabBarItem = [AMTabBarItem itemWithTitle:@"Home" image:kGetImage(@"left-nav-1")];
    vc2.ng_tabBarItem = [AMTabBarItem itemWithTitle:@"Staff" image:kGetImage(@"left-nav-2")];
    vc3.ng_tabBarItem = [AMTabBarItem itemWithTitle:@"Equipment" image:kGetImage(@"left-nav-3")];
    vc4.ng_tabBarItem = [AMTabBarItem itemWithTitle:@"Healthcare Resources" image:kGetImage(@"left-nav-4")];
    vc5.ng_tabBarItem = [AMTabBarItem itemWithTitle:@"Antimicrobial and Drugs" image:kGetImage(@"left-nav-5")];
    vc6.ng_tabBarItem = [AMTabBarItem itemWithTitle:@"Other Clinical Outcomes" image:kGetImage(@"left-nav-6")];
    vc7.ng_tabBarItem = [AMTabBarItem itemWithTitle:@"Return on Investment" image:kGetImage(@"left-nav-7")];
    
//    vc1.ng_tabBarItem.selectedImageTintColor
    
    tabbar = [[BaseTabBarController alloc] initWithDelegate:self];
    tabbar.viewControllers =  @[
                                [[UINavigationController alloc] initWithRootViewController:vc1],
                                [[UINavigationController alloc] initWithRootViewController:vc2],
                                [[UINavigationController alloc] initWithRootViewController:vc3],
                                [[UINavigationController alloc] initWithRootViewController:vc4],
                                [[UINavigationController alloc] initWithRootViewController:vc5],
                                [[UINavigationController alloc] initWithRootViewController:vc6],
                                [[UINavigationController alloc] initWithRootViewController:vc7]
                                ];
    self.window.rootViewController = tabbar;
    [UINavigationBar appearance].hidden = YES;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - tabbar delegate
- (CGSize)tabBarController:(AMTabBarController *)tabBarController sizeOfItemForViewController:(UIViewController *)viewController atIndex:(NSUInteger)index position:(AMTabBarPosition)position{
    if (AMTabBarIsVertical(position)) {
        if (index == 0) {
            return CGSizeMake(135, 107.5);
        }
        return CGSizeMake(135.0f, 80);
    }else{
        return CGSizeMake(60.0f, 49.0f);
    }
}

- (BOOL)tabBarController:(AMTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController atIndex:(NSUInteger)index{
//    if (!tabBarController.selectedIndex) {
//        return NO;
//    }
    return YES;
}

- (void)saveData{
    return;
    UIView *testView = [[UIView alloc] init];
    testView.tag = 111;
    [testView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideSaveView:)]];
    testView.backgroundColor = [UIColor blackColor];
    testView.alpha = 0.3;
    [KEY_WINDOW addSubview:testView];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [testView mas_makeConstraints:^(MASConstraintMaker *make) {
                             make.edges.mas_equalTo(self.window);
                         }];
//                         [testView layoutIfNeeded];
                         
                     }];
}

- (void)hideSaveView:(UITapGestureRecognizer *)tap{
    [tap.view removeFromSuperview];
}

- (void)sendMail{
    
}


@end
