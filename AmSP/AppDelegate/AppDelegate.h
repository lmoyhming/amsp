//
//  AppDelegate.h
//  AmSP
//
//  Created by Lee on 2017/3/9.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

