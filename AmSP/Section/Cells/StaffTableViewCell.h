//
//  StaffTableViewCell.h
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StaffTableViewCell;

@protocol  StaffTableViewCellDelegate <NSObject>

- (void)tableViewCell:(StaffTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField;
- (void)tableViewCell:(StaffTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField;

@end

@interface StaffTableViewCell : UITableViewCell

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, copy) StaffModel *itemData;
@property (nonatomic, assign) id<StaffTableViewCellDelegate> delegate;

@end
