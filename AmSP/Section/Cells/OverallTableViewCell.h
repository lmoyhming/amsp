//
//  OverallTableViewCell.h
//  AmSP
//
//  Created by Lee on 2017/4/7.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrugsModel.h"

@class OverallTableViewCell;
@protocol  OverallTableViewCellDelegate <NSObject>

- (void)tableViewCell:(OverallTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField;
- (void)tableViewCell:(OverallTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField;

@end

@interface OverallTableViewCell : UITableViewCell

@property (nonatomic, strong) DrugsModel *itemData;
@property (nonatomic, assign) id<OverallTableViewCellDelegate> delegate;

@end
