//
//  EquipmentTableViewCell.h
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EquipmentModel.h"

@class EquipmentTableViewCell;

@protocol  EquipmentTableViewCellDelegate <NSObject>

- (void)tableViewCell:(EquipmentTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField;
- (void)tableViewCell:(EquipmentTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField;

@end

@interface EquipmentTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *itemNameLabel;
@property (nonatomic, weak) IBOutlet AMTextField *beforeTxt;
@property (nonatomic, weak) IBOutlet AMTextField *afterTxt;
@property (nonatomic, weak) IBOutlet AMTextField *costTxt;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLeftConstraint;
@property (nonatomic, assign) id<EquipmentTableViewCellDelegate> delegate;


@property (nonatomic, strong) EquipmentModel *itemData;

- (void)hideCost;


@end
