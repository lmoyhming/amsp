//
//  SwitchTableViewCell.m
//  AmSP
//
//  Created by Lee on 2017/4/7.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "SwitchTableViewCell.h"

@interface SwitchTableViewCell () <UITextFieldDelegate>{
    __weak IBOutlet UILabel *itemNameLabel;
    
}

@end

@implementation SwitchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if ([itemNameLabel.text isEqualToString:@"Antimicrobial Drugs Switched from IV to Oral"]) {
        NSLog(@"123");
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setItemData:(DrugsModel *)itemData{
    _itemData = itemData;
    itemNameLabel.text = itemData.itemName;
    _beforeTxt.text = itemData.before ? [itemData.before stringValue] : @"0";
    _afterTxt.text = itemData.after ? [itemData.after stringValue] : @"0";
}

#pragma mark - text field delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.text = @"";
}

- (void)textFieldDidEndEditing:(AMTextField *)textField{
    [textField resignFirstResponder];
    if (_delegate && [_delegate respondsToSelector:@selector(tableViewCell:DidEndEditChartData:)]) {
        if (textField.tag == 1) {
            _itemData.before = @([textField.text integerValue]);
            textField.text = [NSString stringWithFormat:@"%@", textField.text];
        }
        if (textField.tag == 2) {
            _itemData.after = @([textField.text integerValue]);
            if ([_itemData.itemName isEqualToString:@"Antimicrobial Drugs Switched from IV to Oral"]) {
                textField.text = [NSString stringWithFormat:@"%@%%", textField.text];
            }else{
                textField.text = [NSString stringWithFormat:@"%@", textField.text];
            }
            
            
        }
        [_delegate tableViewCell:self DidEndEditChartData:textField];
    }
}

@end
