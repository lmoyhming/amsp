//
//  EquipmentTableViewCell.m
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "EquipmentTableViewCell.h"

@interface EquipmentTableViewCell () <UITextFieldDelegate>{
    
}

@end

@implementation EquipmentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.costTxt.text = @"¥0";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

#pragma mark - text field delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.text = @"";
//    if (_delegate && [_delegate respondsToSelector:@selector(tableViewCell:willBeginEditChartData:)]) {
//        [_delegate tableViewCell:self willBeginEditChartData:textField];
//    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [string isEqualToString:@"."]) {
        return NO;
    }
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [[textField.text componentsSeparatedByString:@"."] lastObject].length == 2 && ![string isEqualToString:@""]) {
        return NO;
    }
    if (textField.text.length == 0 && [string isEqualToString:@"0"]) {
        return NO;
    }
    NSString *numbers = @"01234567890.";
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:numbers] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    BOOL basicTest = [string isEqualToString:filtered];
    if(!basicTest){
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(AMTextField *)textField{
    [textField resignFirstResponder];
    if (_delegate && [_delegate respondsToSelector:@selector(tableViewCell:DidEndEditChartData:)]) {
        if (textField.textType == 1) {
//            _itemData.number = @([textField.text integerValue]);
            textField.text = [NSString stringWithFormat:@"%@", textField.text];
        }
        if (textField.textType == 3) {
//            _itemData.hours = @([textField.text integerValue]);
            textField.text = [NSString stringWithFormat:@"%@%%", textField.text];
        }
        if (textField.textType == 2) {
            _itemData.cost = @([textField.text doubleValue]);
            textField.text = [Global getMoneyStringWithMoneyNumber:[textField.text doubleValue] decimal:YES];
        }
        [_delegate tableViewCell:self DidEndEditChartData:textField];
    }
}

- (void)hideCost{
    _costTxt.hidden = YES;
    _beforeTxt.textType = _afterTxt.textType = 3;
}

@end
