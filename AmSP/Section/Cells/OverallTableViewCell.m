//
//  OverallTableViewCell.m
//  AmSP
//
//  Created by Lee on 2017/4/7.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "OverallTableViewCell.h"

@interface OverallTableViewCell () <UITextFieldDelegate>{
    
    __weak IBOutlet UILabel *itemNameLabel;
    __weak IBOutlet AMTextField *beforeTxt;
    __weak IBOutlet AMTextField *afterTxt;
}

@end

@implementation OverallTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setItemData:(DrugsModel *)itemData{
    _itemData = itemData;
    itemNameLabel.text = itemData.itemName;
    beforeTxt.text = itemData.before ? [itemData.before stringValue] : @"¥0";
    afterTxt.text = itemData.after ? [itemData.after stringValue] : @"¥0";
}

#pragma mark - text field delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.text = @"";
}

@end
