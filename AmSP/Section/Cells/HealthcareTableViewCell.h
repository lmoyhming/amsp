//
//  HealthcareTableViewCell.h
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HealthcareModel.h"

@class HealthcareTableViewCell;

@protocol  HealthcareTableViewCellDelegate <NSObject>

- (void)tableViewCell:(HealthcareTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField;
- (void)tableViewCell:(HealthcareTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField;

@end

@interface HealthcareTableViewCell : UITableViewCell

@property (nonatomic, strong) HealthcareModel *itemData;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) id<HealthcareTableViewCellDelegate> delegate;

@end
