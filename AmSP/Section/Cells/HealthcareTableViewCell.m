//
//  HealthcareTableViewCell.m
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "HealthcareTableViewCell.h"

@interface HealthcareTableViewCell () <UITextFieldDelegate>{
    __weak IBOutlet UILabel *itemNameLabel;
    __weak IBOutlet UITextField *overallTxt;
    __weak IBOutlet UITextField *beforeTxt;
    __weak IBOutlet UITextField *afterTxt;
    __weak IBOutlet UITextField *costTxt;
}

@end

@implementation HealthcareTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setItemData:(HealthcareModel *)itemData{
    _itemData = itemData;
    itemNameLabel.text = itemData.itemName;
    overallTxt.text = itemData.overallNumber ? [itemData.overallNumber stringValue] : @"0";
    beforeTxt.text = itemData.before ? [itemData.before stringValue] : @"0";
    afterTxt.text = itemData.after ? [itemData.after stringValue] : @"0";
    costTxt.text = itemData.cost ? [itemData.cost stringValue] : @"¥0";
}

#pragma mark - text field delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.text = @"";
//    if (_delegate && [_delegate respondsToSelector:@selector(tableViewCell:willBeginEditChartData:)]) {
//        [_delegate tableViewCell:self willBeginEditChartData:textField];
//    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [string isEqualToString:@"."]) {
        return NO;
    }
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [[textField.text componentsSeparatedByString:@"."] lastObject].length == 2 && ![string isEqualToString:@""]) {
        return NO;
    }
    if (textField.text.length == 0 && [string isEqualToString:@"0"]) {
        return NO;
    }
    NSString *numbers = @"01234567890.";
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:numbers] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    BOOL basicTest = [string isEqualToString:filtered];
    if(!basicTest){
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(AMTextField *)textField{
    [textField resignFirstResponder];
    if (_delegate && [_delegate respondsToSelector:@selector(tableViewCell:DidEndEditChartData:)]) {
        if (textField.tag == 1) {
            _itemData.overallNumber = @([textField.text integerValue]);
            textField.text = [NSString stringWithFormat:@"%@", textField.text];
        }
        if (textField.tag == 2) {
            _itemData.before = @([textField.text integerValue]);
            textField.text = [NSString stringWithFormat:@"%@", textField.text];
        }
        if (textField.tag == 3) {
            _itemData.after = @([textField.text integerValue]);
        }
        if (textField.tag == 4) {
            _itemData.cost = @([textField.text doubleValue]);
            textField.text = [Global getMoneyStringWithMoneyNumber:[textField.text doubleValue] decimal:YES];
        }
        [_delegate tableViewCell:self DidEndEditChartData:textField];
    }
}

@end
