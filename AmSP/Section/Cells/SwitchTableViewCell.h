//
//  SwitchTableViewCell.h
//  AmSP
//
//  Created by Lee on 2017/4/7.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrugsModel.h"

@class SwitchTableViewCell;
@protocol  SwitchTableViewCellDelegate <NSObject>

- (void)tableViewCell:(SwitchTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField;
- (void)tableViewCell:(SwitchTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField;

@end

@interface SwitchTableViewCell : UITableViewCell

@property (nonatomic, assign) IBOutlet AMTextField *beforeTxt;
@property (nonatomic, assign) IBOutlet AMTextField *afterTxt;

@property (nonatomic, strong) DrugsModel *itemData;
@property (nonatomic, assign) id<SwitchTableViewCellDelegate> delegate;

@end
