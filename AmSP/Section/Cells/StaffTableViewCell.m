//
//  StaffTableViewCell.m
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "StaffTableViewCell.h"

@interface StaffTableViewCell () <UITextFieldDelegate>{
    __weak IBOutlet UILabel *itemNameLabel;
    __weak IBOutlet UITextField *numberTxt;
    __weak IBOutlet UITextField *hoursTxt;
    __weak IBOutlet UITextField *costTxt;
}

@end

@implementation StaffTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    numberTxt.text = @"0";
    hoursTxt.text = @"0";
    costTxt.text = @"¥0";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setItemData:(StaffModel *)itemData{
    _itemData = itemData;
    itemNameLabel.text = itemData.itemName;
    numberTxt.text = itemData.number ? [itemData.number stringValue] : @"0";
    hoursTxt.text = itemData.hours ? [itemData.hours stringValue] : @"0";
    costTxt.text = itemData.cost ? [itemData.cost stringValue] : @"¥0";
}

#pragma mark - text field delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    textField.text = @"";
    if (_delegate && [_delegate respondsToSelector:@selector(tableViewCell:willBeginEditChartData:)]) {
        [_delegate tableViewCell:self willBeginEditChartData:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [string isEqualToString:@"."]) {
        return NO;
    }
    if ([textField.text rangeOfString:@"."].location != NSNotFound && [[textField.text componentsSeparatedByString:@"."] lastObject].length == 2 && ![string isEqualToString:@""]) {
        return NO;
    }
    if (textField.text.length == 0 && [string isEqualToString:@"0"]) {
        return NO;
    }
    NSString *numbers = @"01234567890.";
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:numbers] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    BOOL basicTest = [string isEqualToString:filtered];
    if(!basicTest){
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    if (_delegate && [_delegate respondsToSelector:@selector(tableViewCell:DidEndEditChartData:)]) {
        if (textField.tag == 1) {
            _itemData.number = @([textField.text integerValue]);
        }
        if (textField.tag == 2) {
            _itemData.hours = @([textField.text integerValue]);
        }
        if (textField.tag == 3) {
            _itemData.cost = @([textField.text doubleValue]);
            textField.text = [Global getMoneyStringWithMoneyNumber:[textField.text doubleValue] decimal:YES];
        }
        [_delegate tableViewCell:self DidEndEditChartData:textField];
    }
}

- (void)endEdit{
    
}

@end
