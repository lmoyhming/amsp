//
//  GuideViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/21.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "GuideViewController.h"

@interface GuideViewController (){
    UIColor *hightColor;
}

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *enterButton;

@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHomeStyle = YES;
    hightColor = self.myTabBarController.tabBar.itemHighlightColor;
    NSString *str1 = @"ECONOMIC EVALUATION OF IMPLEMENTING HOSPITAL";
    NSString *str2 = @"ANTIMICROBIAL STEWARDSHIP PROGRAMS (AmSP)";
    NSString *str3 = @"IN CHINA";
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@\n%@", str1,str2,str3]];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.firstLineHeadIndent = 20;
    [attrString setAttributes:@{
                                NSFontAttributeName: AMFONT(25),
                                NSForegroundColorAttributeName: AMRGB(0x2b759e, 1),
                                NSParagraphStyleAttributeName: style
                                }
                        range:NSMakeRange(0, str1.length)];
    [attrString setAttributes:@{
                                NSFontAttributeName: AMFONT(34),
                                NSForegroundColorAttributeName: AMRGB(0x40a8a0, 1),
                                NSParagraphStyleAttributeName: style
                                }
                        range:NSMakeRange(str1.length + 1, str2.length)];
    [attrString setAttributes:@{
                                NSFontAttributeName: AMFONT(25),
                                NSForegroundColorAttributeName: AMRGB(0x2b759e, 1),
                                NSParagraphStyleAttributeName: style
                                }
                        range:NSMakeRange(str1.length + str2.length + 2, str3.length)];
    _titleLabel.attributedText = attrString;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.myTabBarController.tabBar.itemHighlightColor = [UIColor clearColor];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.myTabBarController.tabBar.itemHighlightColor = hightColor;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"gotoNext"]) {
        self.myTabBarController.tabBar.itemHighlightColor = hightColor;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
