//
//  ViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/9.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isHomeStyle = NO;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.view.backgroundColor = AMClearColor;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClicked:(UIButton *)sender{
    self.myTabBarController.selectedIndex = sender.tag;
}


@end
