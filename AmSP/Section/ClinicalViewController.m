//
//  ClinicalViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "ClinicalViewController.h"

@interface ClinicalViewController () <AMChartDataSource>{
    AMChartView *hospitalChartView;
    AMChartView *mortalityChartView;
    AMChartView *antimicrobialChartView;
    UIScrollView *bgScrollView;
    
    UILabel *hospitalTitlelabel;
    UILabel *mortalityTitleLabel;
    
    NSMutableArray *hospitalArray;
    NSMutableArray *mortalityArray;
    NSMutableArray *antimicrobialArray;
}

@end

@implementation ClinicalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    hospitalArray = [NSMutableArray arrayWithObjects:@[@(0),@(0)],@[@(0),@(0)],@[@(0),@(0)], nil];
    mortalityArray = [NSMutableArray arrayWithObjects:@(0), @(0),nil];
    antimicrobialArray = [NSMutableArray arrayWithObjects:@[@(0),@(0)],@[@(0),@(0)],@[@(0),@(0)],@[@(0),@(0)],@[@(0),@(0)], nil];
    [self configScrollView];
    [self configChartMarkView];
    [self configChartView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)hideKeyboard{
    [self.view endEditing:YES];
}

#pragma mark - keyboard notification
- (void)keyboardWillShow:(NSNotification *)notif{
    CGRect keyboardRect = [notif.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat height = keyboardRect.size.height;
    [bgScrollView setContentSize:CGSizeMake(1, self.view.frame.size.height + height - 64)];
}

- (void)keyboardWillHide:(NSNotification *)notif{
    [bgScrollView setContentSize:CGSizeMake(1, SCREENH_HEIGHT)];
}


#pragma mark - config UI
- (void)configScrollView{
    bgScrollView = [[UIScrollView alloc] init];
    [self.view addSubview:bgScrollView];
    [bgScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [bgScrollView addGestureRecognizer:tap];
}

- (void)configChartMarkView{
    UIView *beforeView = [[UIView alloc] init];
    beforeView.backgroundColor = AMRGB(0xa6a6a6, 1);
    [bgScrollView addSubview:beforeView];
    [beforeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(13, 13));
        make.left.mas_equalTo(30);
        make.top.mas_equalTo(35);
    }];
    
    UILabel *beforeLabel = [[UILabel alloc] init];
    beforeLabel.text = @"before";
    beforeLabel.font = AMFONT(14);
    beforeLabel.textColor = AMRGB(0xa6a6a6, 1);
    [bgScrollView addSubview:beforeLabel];
    [beforeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(beforeView.mas_right).offset(8);
        make.centerY.mas_equalTo(beforeView);
    }];
    
    UIView *afterView = [[UIView alloc] init];
    afterView.backgroundColor = AMRGB(0x558ed5, 1);
    [bgScrollView addSubview:afterView];
    [afterView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(13, 13));
        make.left.mas_equalTo(beforeLabel.mas_right).offset(15);
        make.centerY.mas_equalTo(beforeView);
    }];
    
    UILabel *afterLabel = [[UILabel alloc] init];
    afterLabel.text = @"after";
    afterLabel.font = AMFONT(14);
    afterLabel.textColor = AMRGB(0xa6a6a6, 1);
    [bgScrollView addSubview:afterLabel];
    [afterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(afterView.mas_right).offset(8);
        make.centerY.mas_equalTo(afterView);
    }];
    
    hospitalTitlelabel = [[UILabel alloc] init];
    hospitalTitlelabel.text = @"Hospital Onset Infection";
    hospitalTitlelabel.font = AMFONT_B(18);
    [bgScrollView addSubview:hospitalTitlelabel];
    [hospitalTitlelabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(beforeView);
        make.top.mas_equalTo(beforeView.mas_bottom).offset(20);
    }];
}

- (void)configChartView{
    hospitalChartView = [[AMChartView alloc] initWithFrame:CGRectZero directionType:ChartDirectionTypeV chartName:@""];
    hospitalChartView.datasource = self;
    hospitalChartView.showTxt = YES;
    hospitalChartView.backgroundColor =  AMClearColor;
    [bgScrollView addSubview:hospitalChartView];
    [hospitalChartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgScrollView);
        make.top.mas_equalTo(hospitalTitlelabel.mas_bottom).offset(15);
        make.width.mas_equalTo(530);
        make.height.mas_equalTo(230);
    }];
    [hospitalChartView show];
    
    mortalityTitleLabel = [[UILabel alloc] init];
    mortalityTitleLabel.text = @"Mortality";
    mortalityTitleLabel.font = AMFONT_B(18);
    [bgScrollView addSubview:mortalityTitleLabel];
    [mortalityTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(hospitalChartView.mas_right).offset(35);
        make.top.mas_equalTo(bgScrollView).offset(45);
    }];
    
    mortalityChartView = [[AMChartView alloc] initWithFrame:CGRectZero directionType:ChartDirectionTypeV chartName:@""];
    mortalityChartView.datasource = self;
    mortalityChartView.showTxt = YES;
    mortalityChartView.isMortality = YES;
    mortalityChartView.backgroundColor = AMClearColor;
    [bgScrollView addSubview:mortalityChartView];
    [mortalityChartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mortalityTitleLabel);
        make.right.mas_equalTo(self.view).offset(-35);
        make.top.mas_equalTo(mortalityTitleLabel.mas_bottom).offset(10);
        make.bottom.mas_equalTo(hospitalChartView).offset(25);
    }];
    [mortalityChartView show];
    
    UILabel *antimicrobialTitleLabel = [[UILabel alloc] init];
    antimicrobialTitleLabel.font = AMFONT_B(18);
    antimicrobialTitleLabel.text = @"Antimicrobial Resistance";
    [bgScrollView addSubview:antimicrobialTitleLabel];
    [antimicrobialTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(hospitalTitlelabel);
        make.top.mas_equalTo(hospitalChartView.mas_bottom).offset(20);
    }];
    
    antimicrobialChartView = [[AMChartView alloc] initWithFrame:CGRectZero directionType:ChartDirectionTypeV chartName:@""];
    antimicrobialChartView.datasource = self;
    antimicrobialChartView.showTxt = YES;
    antimicrobialChartView.backgroundColor = AMClearColor;
    [bgScrollView addSubview:antimicrobialChartView];
    [antimicrobialChartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(bgScrollView);
        make.top.mas_equalTo(antimicrobialTitleLabel.mas_bottom).offset(20);
        make.height.mas_equalTo(hospitalChartView).multipliedBy(1.2);
        make.right.mas_equalTo(mortalityChartView);
    }];
    [antimicrobialChartView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - chart view delegate
- (NSArray *)nameArrayInChartView:(AMChartView *)chartView{
    if (chartView == hospitalChartView) {
        return @[@"Methicillin-resistant\nstaphylococcus aureus\n(MRSA)infection", @"Carbapenem-resistant\nEnterobacteriaceae\n(CRE)infection", @"Carbapenem-resistant A.\nbaumannii(CRAB)\ninfection"];
    }else if (chartView == mortalityChartView) {
        return @[@"Before AmSP", @"After AmSP"];
    }else{
        return @[@"MRSA",@"Extended-spectrum\nβ-lactamase producing\nEscherichia coli",@"Ciprofloxacin-resistant\nEscherichia coli",@"Imipenemresistant\nPseudomonas aeruginosa",@"Imipenem-resistant\nAcinetobacter\nbaumannii"];
    }
    
}

- (NSArray *)valueArrayForChartView:(AMChartView *)chartView{
    if (chartView == hospitalChartView) {
        return hospitalArray;
    }else if (chartView == mortalityChartView) {
        return mortalityArray;
    }else{
        return antimicrobialArray;
    }
    
}

- (NSArray *)colorArrayInChartView:(AMChartView *)chartView{
    if (chartView == mortalityChartView) {
        return @[AMRGB(0xa6a6a6, 1), AMRGB(0x558ed5, 1)];
    }
    return @[
             AMRGB(0x057cb2, 1),
             AMRGB(0x529fc6, 1),
             AMRGB(0x6caccd, 1),
             AMRGB(0x91c0d9, 1),
             AMRGB(0xb5d4e5, 1),
             AMRGB(0xe0edf4, 1)];
}

- (NSUInteger)axisLineSectionCountInChartView:(AMChartView *)chartView{
    if (chartView == hospitalChartView) {
        return 3;
    }else if (chartView == mortalityChartView) {
        return 5;
    }else{
        return 5;
    }
}

- (CGFloat)axisLineMaxValueInChartView:(AMChartView *)chartView{
    if (chartView == hospitalChartView) {
        return 15;
    }else if (chartView == mortalityChartView) {
        return 4.4;
    }else{
        return 100;
    }
}

- (CGFloat)axisLineStepInChartView:(AMChartView *)chartView{
    if (chartView == hospitalChartView) {
        return 5;
    }else if (chartView == mortalityChartView) {
        return 0.2;
    }else{
        return 20;
    }
}

- (CGFloat)chartBarHeightInChartView:(AMChartView *)chartView{
    return 54;
}

- (UIEdgeInsets)chartViewPaddingInChartView:(AMChartView *)chartView{
    if (chartView == hospitalChartView) {
        return UIEdgeInsetsMake(58, 90, 70, 0);
    }else if (chartView == mortalityChartView) {
        return UIEdgeInsetsMake(58, 70, 40, 0);
    }else{
        return UIEdgeInsetsMake(58, 90, 70, 0);
    }
}

- (void)endEditValueWithChartView:(AMChartView *)chartView value:(CGFloat)value{
    if (chartView == hospitalChartView) {
        [hospitalArray removeAllObjects];
        for (int i = 0; i < 3; i++) {
            NSMutableArray *array = [NSMutableArray array];
            for (int j = 0; j < 2; j++) {
                AMTextField *txt = [chartView viewWithTag:i*2+j + 1];
                if (txt) {
                    [array addObject:@([txt.text floatValue])];
                    if ([txt.text rangeOfString:@"%"].location == NSNotFound) {
                        txt.text = [txt.text stringByAppendingString:@"%"];
                    }
                    
                }
            }
            [hospitalArray addObject:array];
        }
    }
    if (chartView == mortalityChartView) {
        [mortalityArray removeAllObjects];
        for (int i = 0; i < 2; i++) {
            AMTextField *txt = [chartView viewWithTag:(i + 1) * 10];
            if (txt) {
                [mortalityArray addObject:@([txt.text floatValue])];
            }
        }
    }
    if (chartView == antimicrobialChartView) {
        [antimicrobialArray removeAllObjects];
        for (int i = 0; i < 5; i++) {
            NSMutableArray *array = [NSMutableArray array];
            for (int j = 0; j < 2; j++) {
                AMTextField *txt = [chartView viewWithTag:i*2+j + 1];
                if (txt) {
                    [array addObject:@([txt.text floatValue])];
                    if ([txt.text rangeOfString:@"%"].location == NSNotFound) {
                        txt.text = [txt.text stringByAppendingString:@"%"];
                    }
                    
                }
            }
            [antimicrobialArray addObject:array];
        }
    }
    [chartView show];
}

@end
