//
//  EquipmentViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "EquipmentViewController.h"

#import "EquipmentTableViewCell.h"


@interface EquipmentViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, AMChartDataSource, EquipmentTableViewCellDelegate>{
    __weak IBOutlet UITableView *tabView;
    __weak IBOutlet UITextField *totalTxt;
    
    AMChartView *chartView;
    
    NSMutableArray *equipmentData;
    NSArray *itemNameArray;
}

@end

@implementation EquipmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    itemNameArray = @[
                      @"Susceptibility test:",
                      @"Test supplies",
                      @"Test equipment",
                      @"Other tests:",
                      @"Test supplies",
                      @"Test equipment",
                      @"Data collection or communication system"
                      ];
    equipmentData = [NSMutableArray array];
    NSArray *testArray = @[@{
                               @"itemName": @"Susceptibility test:",
                               @"sixtyNumber": @"3",
                               @"eightyNumber": @"9",
                               @"cost": @"25.42",
                               },
                           @{
                               @"itemName": @"Test supplies ",
                               @"sixtyNumber": @"8",
                               @"eightyNumber": @"10",
                               @"cost": @"33.58",
                               },
                           @{
                               @"itemName": @"Nurse",
                               @"sixtyNumber": @"3",
                               @"eightyNumber": @"9",
                               @"cost": @"25.94",
                               },
                           @{
                               @"itemName": @"Pharmacist",
                               @"sixtyNumber": @"1",
                               @"eightyNumber": @"23",
                               @"cost": @"22.68",
                               },
                           @{
                               @"itemName": @"Microbiologist",
                               @"sixtyNumber": @"1",
                               @"eightyNumber": @"15",
                               @"cost": @"29.53",
                               },
                           @{
                               @"itemName": @"Lab technician",
                               @"sixtyNumber": @"1",
                               @"eightyNumber": @"18",
                               @"cost": @"29.87",
                               }
                           ];
    for (int i = 0; i < testArray.count; i++) {
        NSDictionary *dict = testArray[i];
        NSError *err;
        EquipmentModel *model = [[EquipmentModel alloc] initWithDictionary:dict error:&err];
        if (!err) {
            [equipmentData addObject:model];
        }
    }
    [self configChartViewUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configChartViewUI{
    chartView = [[AMChartView alloc] initWithFrame:CGRectZero directionType:ChartDirectionTypeH chartName:@"Equipment Cost"];
    chartView.showMark = YES;
    chartView.datasource = self;
    chartView.topPadding = 20;
    chartView.backgroundColor =  AMRGB(0xfcfcfc, 1);
    [self.view addSubview:chartView];
    [chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tabView.mas_bottom).offset(10);
        make.right.left.mas_equalTo(self.view).offset(0);
        make.bottom.mas_equalTo(self.bottomView.mas_top).offset(-5);
    }];
    [chartView show];
}

#pragma mark - chart view delegate
- (NSArray *)nameArrayInChartView:(AMChartView *)chartView{
    return @[@"Susceptibility test", @"Other tests", @"Data collection or\n communication system"];
}

- (NSArray *)valueArrayForChartView:(AMChartView *)chartView{
    
    return @[@[@(37000),@(43000)],@[@(37000),@(43000)],@[@(37000),@(43000)]];
    NSMutableArray *valuesArray = [NSMutableArray array];
    for (int i = 0; i < equipmentData.count; i++) {
        EquipmentModel *model = equipmentData[i];
        [valuesArray addObject:@([model.sixtyNumber integerValue] * 10000)];
    }
    return valuesArray;
}

- (NSArray *)colorArrayInChartView:(AMChartView *)chartView{
    return @[
             AMRGB(0x057cb2, 1),
             AMRGB(0x529fc6, 1),
             AMRGB(0x6caccd, 1),
             AMRGB(0x91c0d9, 1),
             AMRGB(0xb5d4e5, 1),
             AMRGB(0xe0edf4, 1)];
}

- (NSUInteger)axisLineSectionCountInChartView:(AMChartView *)chartView{
    return 3;
}

- (CGFloat)axisLineMaxValueInChartView:(AMChartView *)chartView{
    return 60000;
}

- (CGFloat)axisLineStepInChartView:(AMChartView *)chartView{
    return 20000;
}

- (UIEdgeInsets)chartViewPaddingInChartView:(AMChartView *)chartView{
    return UIEdgeInsetsMake(30, 214, 40, 185);
}

#pragma mark - table view delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"equipmentHeaderCell"];
        return cell;
    }else{
        EquipmentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"equipmentCell"];
        cell.delegate = self;
        cell.itemNameLabel.text = itemNameArray[indexPath.row - 1];
        if (indexPath.row == 1 || indexPath.row == 4) {
            cell.afterTxt.textType = 2;
            [cell hideCost];
        }
        if (indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 6) {
            cell.titleLeftConstraint.constant = 53;
            
        }else{
            cell.titleLeftConstraint.constant = 38;
        }
        if (indexPath.row == 2 || indexPath.row == 5) {
            cell.beforeTxt.userInteractionEnabled = NO;
            cell.afterTxt.userInteractionEnabled = NO;
            cell.beforeTxt.textColor = AMRGB(0xa6a6a6, 1);
            cell.afterTxt.textColor = AMRGB(0xa6a6a6, 1);
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 50;
    }else{
        return (tableView.frame.size.height - 50) / 7 + 0.1f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
}

#pragma mark - 
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - staff cell delegate
- (void)tableViewCell:(EquipmentTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField{
//    currentTxt = txtField;
}

- (void)tableViewCell:(EquipmentTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField{
    [txtField resignFirstResponder];
    NSLog(@"%@", cell.itemData);
//    [staffData replaceObjectAtIndex:cell.index withObject:cell.itemData];
    [chartView show];
}

@end
