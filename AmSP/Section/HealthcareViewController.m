//
//  HealthcareViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "HealthcareViewController.h"

#import "HealthcareModel.h"
#import "HealthcareTableViewCell.h"

@interface HealthcareViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, AMChartDataSource, HealthcareTableViewCellDelegate>{
    __weak IBOutlet UITableView *tabView;
    __weak IBOutlet UITextField *totalTxt;
    __weak IBOutlet UIView *totalView;
    AMChartView *chartView;
    NSArray *itemNameArray;
    NSMutableArray *healthcareData;
}

@end

@implementation HealthcareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    itemNameArray = @[@"General ward",@"ICU-bay bed",@"ICU-single room"];
    healthcareData = [NSMutableArray array];
    for (int i = 0; i < 3; i++) {
        HealthcareModel *model = [[HealthcareModel alloc] init];
        model.itemName = itemNameArray[i];
        [healthcareData addObject:model];
    }
    [self configChartViewUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configChartViewUI{
    chartView = [[AMChartView alloc] initWithFrame:CGRectZero directionType:ChartDirectionTypeH chartName:@"Healthcare Resources"];
    chartView.showMark = YES;
    chartView.datasource = self;
    chartView.backgroundColor =  AMRGB(0xfcfcfc, 1);
//    [self.view addSubview:chartView];
    [self.view insertSubview:chartView belowSubview:totalView];
    [chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tabView.mas_bottom).offset(30);
        make.left.mas_equalTo(self.view);
        make.right.mas_equalTo(totalView.mas_left);
        make.bottom.mas_equalTo(self.bottomView.mas_top).offset(-5);
    }];
    [chartView show];
    
    [totalView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(chartView);
        make.left.mas_equalTo(chartView.mas_right);
        make.right.mas_equalTo(self.view);
    }];
}

#pragma mark - chart view delegate
- (NSArray *)nameArrayInChartView:(AMChartView *)chartView{
    
    return itemNameArray;
}

- (NSArray *)valueArrayForChartView:(AMChartView *)chartView{
    NSMutableArray *valuesArray = [NSMutableArray array];
    for (int i = 0; i < healthcareData.count; i++) {
        HealthcareModel *model = healthcareData[i];
        NSMutableArray *sectionValueArray = [NSMutableArray array];
        CGFloat beforeValue = [model.overallNumber doubleValue] * [model.before integerValue] * [model.cost integerValue];
        CGFloat afterValue = [model.overallNumber doubleValue] * [model.after integerValue] * [model.cost integerValue];
        [sectionValueArray addObject:@(beforeValue)];
        [sectionValueArray addObject:@(afterValue)];
        [valuesArray addObject:sectionValueArray];
    }
    NSLog(@"%@", valuesArray);
    return valuesArray;
}

- (NSArray *)colorArrayInChartView:(AMChartView *)chartView{
    return @[
             AMRGB(0x057cb2, 1),
             AMRGB(0x529fc6, 1),
             AMRGB(0x6caccd, 1),
             AMRGB(0x91c0d9, 1),
             AMRGB(0xb5d4e5, 1),
             AMRGB(0xe0edf4, 1)];
}

- (NSUInteger)axisLineSectionCountInChartView:(AMChartView *)chartView{
    return 3;
}

- (CGFloat)axisLineMaxValueInChartView:(AMChartView *)chartView{
    return 150000;
}

- (CGFloat)axisLineStepInChartView:(AMChartView *)chartView{
    return 50000;
}

- (UIEdgeInsets)chartViewPaddingInChartView:(AMChartView *)chartView{
    return UIEdgeInsetsMake(60, 185, 90, 135);
}

#pragma mark - table view delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"healthcareHeaderCell"];
        return cell;
    }else{
        HealthcareTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"healthcareCell"];
        cell.delegate = self;
        cell.index = indexPath.row - 1;
        cell.itemData = healthcareData[indexPath.row - 1];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 60;
    }else{
        return (tableView.frame.size.height - 60) / 3 + 0.2f;
    }
}

#pragma mark - staff cell delegate
- (void)tableViewCell:(HealthcareTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField{
    
}

- (void)tableViewCell:(HealthcareTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField{
    [self.view endEditing:YES];
    [healthcareData replaceObjectAtIndex:cell.index withObject:cell.itemData];
    [chartView show];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

@end
