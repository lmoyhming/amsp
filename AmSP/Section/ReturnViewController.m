//
//  ReturnViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "ReturnViewController.h"

@interface ReturnViewController (){
    __weak IBOutlet UIImageView *leftArrow;
    __weak IBOutlet UIImageView *rightArrow;
    
    __weak IBOutlet UIView *investmentBar;
    __weak IBOutlet UIView *returnBar;
    
    __weak IBOutlet UIView *staffBar;
    __weak IBOutlet UIView *equipmentBar;
    __weak IBOutlet UIView *hospitalizationBar;
    __weak IBOutlet UIView *drugBar;
    
    __weak IBOutlet UITextField *benefitLabel;
    __weak IBOutlet UITextField *returnLabel;
    
}

@end

@implementation ReturnViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    benefitLabel.layer.borderWidth = 1;
    benefitLabel.layer.cornerRadius = 6;
    benefitLabel.layer.borderColor = AMRGB(0x789241, 1).CGColor;
    benefitLabel.backgroundColor = [UIColor lightGrayColor];
    benefitLabel.layer.masksToBounds = YES;
    [self configTextFieldInset:benefitLabel];
    
    returnLabel.layer.borderWidth = 1;
    returnLabel.layer.cornerRadius = 6;
    returnLabel.layer.borderColor = AMRGB(0x789241, 1).CGColor;
    returnLabel.backgroundColor = [UIColor lightGrayColor];
    returnLabel.layer.masksToBounds = YES;
    [self configTextFieldInset:returnLabel];
}

- (void)configTextFieldInset:(UITextField *)txtField{
    txtField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    txtField.leftViewMode = UITextFieldViewModeAlways;
    
    txtField.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    txtField.rightViewMode = UITextFieldViewModeAlways;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
