//
//  StaffViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/9.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "StaffViewController.h"

#import "StaffTableViewCell.h"

@interface StaffViewController ()<AMChartDataSource, UITableViewDelegate, UITableViewDataSource, StaffTableViewCellDelegate>{
    AMChartView *chartView;
    __weak IBOutlet UITableView *tabView;
    UITextField *currentTxt;
    
    NSMutableArray *staffData;
}

@end

@implementation StaffViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = AMRGB(0xf6f6f6, 1);
    
    AMViewShadow(tabView, CGSizeMake(1,1));
    
    staffData = [NSMutableArray array];
    
    NSArray *names = @[@"Program coordinator",@"Physician",@"Nurse",@"Pharmacist",@"Microbiologist",@"Lab technician"];
//    NSArray *testArray = @[@{
//                               @"itemName": @"Program coordinator",
//                               @"number": @"3",
//                               @"hours": @"9",
//                               @"cost": @"25.42",
//                               },
//                           @{
//                               @"itemName": @"Physician",
//                               @"number": @"8",
//                               @"hours": @"10",
//                               @"cost": @"33.58",
//                               },
//                           @{
//                               @"itemName": @"Nurse",
//                               @"number": @"3",
//                               @"hours": @"9",
//                               @"cost": @"25.94",
//                               },
//                           @{
//                               @"itemName": @"Pharmacist",
//                               @"number": @"1",
//                               @"hours": @"23",
//                               @"cost": @"22.68",
//                               },
//                           @{
//                               @"itemName": @"Microbiologist",
//                               @"number": @"1",
//                               @"hours": @"15",
//                               @"cost": @"29.53",
//                               },
//                           @{
//                               @"itemName": @"Lab technician",
//                               @"number": @"1",
//                               @"hours": @"18",
//                               @"cost": @"29.87",
//                               }
//                           ];
    for (int i = 0; i < names.count; i++) {
        
        StaffModel *model = [[StaffModel alloc] init];
        model.itemName = names[i];
        [staffData addObject:model];
    }
    [self configChartViewUI];
    
}

- (void)configChartViewUI{
    chartView = [[AMChartView alloc] initWithFrame:CGRectZero directionType:ChartDirectionTypeH chartName:@"AmSP Cost"];
    AMViewShadow(chartView, CGSizeMake(1, 1));
    chartView.datasource = self;
    chartView.topPadding = 20;
    chartView.backgroundColor =  AMRGB(0xfcfcfc, 1);
    [self.view addSubview:chartView];
    [chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(tabView.mas_bottom).offset(10);
        make.right.left.mas_equalTo(self.view).offset(0);
        make.bottom.mas_equalTo(self.bottomView.mas_top).offset(-5);
    }];
    [chartView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - chart view delegate
- (NSArray *)nameArrayInChartView:(AMChartView *)chartView{
    NSMutableArray *namesArray = [NSMutableArray array];
    for (int i = 0; i < staffData.count; i++) {
        StaffModel *model = staffData[i];
        [namesArray addObject:model.itemName];
    }
    return namesArray;
}

- (NSArray *)valueArrayForChartView:(AMChartView *)chartView{
    NSMutableArray *valuesArray = [NSMutableArray array];
    for (int i = 0; i < staffData.count; i++) {
        StaffModel *model = staffData[i];
        CGFloat cost = [model.number integerValue] * [model.hours doubleValue] * [model.cost doubleValue] * 52;
        [valuesArray addObject:@(cost)];
    }
    return valuesArray;
    //return @[@(50000),@(200000)];//@[@"0", @"50,000", @"100,000", @"150,000", @"200,000", @"250,000"];
}

- (NSArray *)colorArrayInChartView:(AMChartView *)chartView{
    return @[
             AMRGB(0x057cb2, 1),
             AMRGB(0x529fc6, 1),
             AMRGB(0x6caccd, 1),
             AMRGB(0x91c0d9, 1),
             AMRGB(0xb5d4e5, 1),
             AMRGB(0xe0edf4, 1)];
}

- (NSUInteger)axisLineSectionCountInChartView:(AMChartView *)chartView{
    return 5;
}

- (CGFloat)axisLineMaxValueInChartView:(AMChartView *)chartView{
    return 250000;
}

- (CGFloat)axisLineStepInChartView:(AMChartView *)chartView{
    return 50000;
}

- (CGFloat)chartBarHeightInChartView:(AMChartView *)chartView{
    return 20;
}

- (UIEdgeInsets)chartViewPaddingInChartView:(AMChartView *)chartView{
    return UIEdgeInsetsMake(20, 215, 40, 135);
}

#pragma mark - table view delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return staffData.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"staffHeaderCell"];
        return cell;
    }else{
        StaffTableViewCell *cell = [tableView  dequeueReusableCellWithIdentifier:@"staffCell"];
        StaffModel *model = staffData[indexPath.row - 1];
        cell.delegate = self;
        cell.index = indexPath.row - 1;
        cell.itemData = model;
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.frame.size.height / 7 + 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [currentTxt resignFirstResponder];
}

#pragma mark - staff cell delegate
- (void)tableViewCell:(StaffTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField{
    currentTxt = txtField;
}

- (void)tableViewCell:(StaffTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField{
    [txtField resignFirstResponder];
    NSLog(@"%@", cell.itemData);
    [staffData replaceObjectAtIndex:cell.index withObject:cell.itemData];
    [chartView show];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [currentTxt resignFirstResponder];
}

@end
