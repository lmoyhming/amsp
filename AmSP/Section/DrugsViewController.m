//
//  DrugsViewController.m
//  AmSP
//
//  Created by Lee on 2017/3/22.
//  Copyright © 2017年 Pharmerit. All rights reserved.
//

#import "DrugsViewController.h"
#import "AMChartMarkView.h"
#import "OverallTableViewCell.h"
#import "SwitchTableViewCell.h"

#import "DrugsModel.h"

@interface DrugsViewController () <UITableViewDelegate, UITableViewDataSource, AMChartDataSource, SwitchTableViewCellDelegate>{
    __weak IBOutlet UITableView *overallTabView;
    __weak IBOutlet UITableView *bottomTabView;
    __weak IBOutlet AMTextField *totalTxt;
    __weak IBOutlet UIView *topBgView;
    __weak IBOutlet UIScrollView *bgScrollView;
    
    AMChartView *chartView;
    
    NSMutableArray *overallArray;
    NSMutableArray *switchArray;
}

@end

@implementation DrugsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configData];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [bgScrollView addGestureRecognizer:tap];
    overallTabView.estimatedRowHeight = 44;
    overallTabView.rowHeight = UITableViewAutomaticDimension;
    bottomTabView.estimatedRowHeight = 44;
    bottomTabView.rowHeight = UITableViewAutomaticDimension;
    AMViewShadow(topBgView, CGSizeMake(0, 1));
    AMViewShadow(overallTabView, CGSizeMake(1, 1));
    bottomTabView.tableFooterView = [UIView new];
    bottomTabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self configChartViewUI];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)hideKeyboard{
    [self.view endEditing:YES];
}

- (void)configData{
    overallArray = [NSMutableArray array];
    switchArray = [NSMutableArray array];
    
    for (int i = 0; i < 2; i++) {
        DrugsModel *model = [[DrugsModel alloc] init];
        if (i == 0) {
            model.itemName = @"Antimicrobial drugs";
        }else{
            model.itemName = @"Other drugs";
        }
        [overallArray addObject:model];
    }
    for (int i = 0; i < 4; i++) {
        DrugsModel *model = [[DrugsModel alloc] init];
        switch (i) {
            case 0:
                model.itemName = @"Antimicrobial Drugs Switched from IV to Oral";
                break;
            case 1:
                model.itemName = @"Restricted antimicrobial drugs use";
                break;
            case 2:
                model.itemName = @"Non-restricted antimicrobial drugs use";
                break;
            case 3:
                model.itemName = @"Total antimicrobial use";
                break;
            default:
                break;
        }
        [switchArray addObject:model];
    }
}

#pragma mark - keyboard notification
- (void)keyboardWillShow:(NSNotification *)notif{
    NSLog(@"%@", notif.userInfo);
    CGRect keyboardRect = [notif.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat height = keyboardRect.size.height;
    [bgScrollView setContentSize:CGSizeMake(1, self.view.frame.size.height + height - 64)];
}

- (void)keyboardWillHide:(NSNotification *)notif{
    NSLog(@"%@", notif.userInfo);
    [bgScrollView setContentSize:CGSizeMake(1, SCREENH_HEIGHT)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configChartViewUI{
    chartView = [[AMChartView alloc] initWithFrame:CGRectZero directionType:ChartDirectionTypeH chartName:@"Cost"];
    
    chartView.showMark = NO;
    chartView.datasource = self;
    chartView.backgroundColor = AMClearColor;
//    chartView.backgroundColor =  AMRGB(0xfcfcfc, 1);
    [overallTabView.superview addSubview:chartView];
    [chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(overallTabView.mas_right);
        make.top.mas_equalTo(overallTabView).offset(-10);
        make.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(overallTabView.superview.mas_bottom);
    }];
    [chartView show];
    
    AMChartMarkView *markView = [[AMChartMarkView alloc] init];
    [overallTabView.superview addSubview:markView];
    [markView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(totalTxt);
        make.right.mas_equalTo(overallTabView.superview);
        make.size.mas_equalTo(CGSizeMake(100, 50));
    }];
    
}

#pragma mark - table view delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == overallTabView) {
        return 1;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == overallTabView) {
        return 3;
    }else{
        if (section == 0) {
            return 2;
        }else{
            return 4;
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == overallTabView) {
        if (indexPath.row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"overallHeaderCell"];
            return cell;
        }else{
            OverallTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"overallCell"];
            cell.itemData = overallArray[indexPath.row - 1];
            return cell;
        }
    }else{
        if (indexPath.row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"switchHeaderCell"];
            if (indexPath.section == 1) {
                UILabel *label = [cell.contentView viewWithTag:1];
                label.text = @"DDD per 100 patients treated\nAntimicrobial";
            }
            return cell;
        }else{
            SwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"switchCell"];
            cell.delegate = self;
            if (indexPath.section == 0) {
                cell.afterTxt.textType = 3;
            }
            cell.itemData = switchArray[indexPath.row - 1 + (indexPath.section * 1)];
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    view.tintColor = AMClearColor;
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section{
    view.tintColor = AMClearColor;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == overallTabView) {
        return;
    }
    // 圆角弧度半径
    CGFloat cornerRadius = 0.f;
    // 设置cell的背景色为透明，如果不设置这个的话，则原来的背景色不会被覆盖
    cell.backgroundColor = UIColor.clearColor;
    
    // 创建一个shapeLayer
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    CAShapeLayer *backgroundLayer = [[CAShapeLayer alloc] init]; //显示选中
    // 创建一个可变的图像Path句柄，该路径用于保存绘图信息
    CGMutablePathRef pathRef = CGPathCreateMutable();
    // 获取cell的size
    // 第一个参数,是整个 cell 的 bounds, 第二个参数是距左右两端的距离,第三个参数是距上下两端的距离
    CGRect bounds = CGRectInset(cell.bounds, 10, 0);
    
    // CGRectGetMinY：返回对象顶点坐标
    // CGRectGetMaxY：返回对象底点坐标
    // CGRectGetMinX：返回对象左边缘坐标
    // CGRectGetMaxX：返回对象右边缘坐标
    // CGRectGetMidX: 返回对象中心点的X坐标
    // CGRectGetMidY: 返回对象中心点的Y坐标
    
    // 这里要判断分组列表中的第一行，每组section的第一行，每组section的中间行
    
    // CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
    if (indexPath.row == 0) {
        // 初始起点为cell的左下角坐标
        CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
        // 起始坐标为左下角，设为p，（CGRectGetMinX(bounds), CGRectGetMinY(bounds)）为左上角的点，设为p1(x1,y1)，(CGRectGetMidX(bounds), CGRectGetMinY(bounds))为顶部中点的点，设为p2(x2,y2)。然后连接p1和p2为一条直线l1，连接初始点p到p1成一条直线l，则在两条直线相交处绘制弧度为r的圆角。
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
        // 终点坐标为右下角坐标点，把绘图信息都放到路径中去,根据这些路径就构成了一块区域了
        CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
        CGPathCloseSubpath(pathRef);
        
    } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
        // 初始起点为cell的左上角坐标
        CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
        CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
        // 添加一条直线，终点坐标为右下角坐标点并放到路径中去
        CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
    } else {
        // 添加cell的rectangle信息到path中（不包括圆角）
        CGPathAddRect(pathRef, nil, bounds);
    }
    // 把已经绘制好的可变图像路径赋值给图层，然后图层根据这图像path进行图像渲染render
    layer.path = pathRef;
    backgroundLayer.path = pathRef;
    // 注意：但凡通过Quartz2D中带有creat/copy/retain方法创建出来的值都必须要释放
    CFRelease(pathRef);
    // 按照shape layer的path填充颜色，类似于渲染render
    //     layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
    layer.fillColor = [UIColor whiteColor].CGColor;
    layer.strokeColor = [UIColor lightGrayColor].CGColor;
    
    // view大小与cell一致
    UIView *roundView = [[UIView alloc] initWithFrame:bounds];
    // 添加自定义圆角后的图层到roundView中
    [roundView.layer insertSublayer:layer atIndex:0];
    roundView.backgroundColor = UIColor.clearColor;
    // cell的背景view
    cell.backgroundView = roundView;
    
    // 以上方法存在缺陷当点击cell时还是出现cell方形效果，因此还需要添加以下方法
    // 如果你 cell 已经取消选中状态的话,那以下方法是不需要的.
    UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
    backgroundLayer.fillColor = [UIColor cyanColor].CGColor;
    [selectedBackgroundView.layer insertSublayer:backgroundLayer atIndex:0];
    selectedBackgroundView.backgroundColor = UIColor.clearColor;
    cell.selectedBackgroundView = selectedBackgroundView;
}

#pragma mark - chart view delegate
- (NSArray *)nameArrayInChartView:(AMChartView *)chartView{
    
    return @[@"Other drugs", @"Antimicrobial drugs"];
}

- (NSArray *)valueArrayForChartView:(AMChartView *)chartView{
    NSMutableArray *valuesArray = [NSMutableArray array];
    return @[@[@(45000),@(35000)],@[@(25000),@(40000)]];
}

- (NSArray *)colorArrayInChartView:(AMChartView *)chartView{
    return @[
             AMRGB(0x057cb2, 1),
             AMRGB(0x529fc6, 1),
             AMRGB(0x6caccd, 1),
             AMRGB(0x91c0d9, 1),
             AMRGB(0xb5d4e5, 1),
             AMRGB(0xe0edf4, 1)];
}

- (NSUInteger)axisLineSectionCountInChartView:(AMChartView *)chartView{
    return 2;
}

- (CGFloat)axisLineMaxValueInChartView:(AMChartView *)chartView{
    return 100000;
}

- (CGFloat)axisLineStepInChartView:(AMChartView *)chartView{
    return 50000;
}

- (UIEdgeInsets)chartViewPaddingInChartView:(AMChartView *)chartView{
    return UIEdgeInsetsMake(10, 180, 50, 80);
}

#pragma mark - cell delegate
- (void)tableViewCell:(SwitchTableViewCell *)cell willBeginEditChartData:(UITextField *)txtField{
    
}

- (void)tableViewCell:(SwitchTableViewCell *)cell DidEndEditChartData:(UITextField *)txtField{
    
}



@end
